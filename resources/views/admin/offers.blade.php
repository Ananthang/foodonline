@extends('layouts.homenav')


@section('content')
  <br>
                    <br>
                    <br>
    <ul class="nav nav-tabs nav-justified blue" role="tablist">
    <li class="nav-item ">
        <a class="nav-link active" data-toggle="tab" href="#fooditems" role="tab">
            Food Items
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" data-toggle="tab" href="#currentOffer" role="tab">
            Current Offered Items
        </a>
    </li>
</ul>
<!-- /tab panel list-->
<!--tap panel content -->
<div class="tab-content card ">
    <!--tap panel content show description-->
    <div class="tab-pane fade  show active" id="fooditems" role="tabpanel">
        <br>

          <p>
             <table class="table table-striped table-bordered table-hover offerupdate" id="Table1Id">
                <thead class="">
                    <tr class="">
                        <th>Food Item id</th>
                        <th>Food Name</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Size</th>
                        <th>Price (Rs.)</th>
                        <th>Add Offer</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($foods as $food)

                    <tr>
                        <td>{{$food->id}}</td>
                        <td>{{$food->foodname}}</td>
                        <td>
                            <img src="/storage/upload/foodImage/{{$food->foodImg}}" id="showImage" class="img-fluid" alt="{{$food->foodname}}"></img>
                        </td>
                        <td>{{$food->description}}</td>
                        <td>{{$food->typename}}</td>
                        <td>{{$food->size_name}}</td>
                        <td id="price{{$food->id}}" data-id="{{$food->price}}">
                          <input type="text" class = "form-control" id = "input{{$food->id}}" placeholder="{{$food->price}}">  
                        </td>
                        <td>
                        <a class="btn btn-success addoffer" data-id = "{{ $food->id }}">Add Offer</a>
                        </td>
                        
                        


                    </tr>
                    @endforeach
                </tbody>

            </table>
        </p>

    </div>

    <div class="tab-pane fade" id="currentOffer" role="tabpanel">
        <br>
        <p>
             <table class="table table-striped table-bordered table-hover offerupdate1" id="TableId">
                <thead class="">
                    <tr class="">
                        <th>Food Item id</th>
                        <th>Food Name</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Size</th>
                        <th>Before Offer Price</th>
                        <th>Price</th>
                        <th>Withdraw Offer</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($offers as $offer)

                    <tr>
                        <td>{{$offer->id}}</td>
                        <td>{{$offer->foodname}}</td>
                        <td>
                            <img src="/storage/upload/foodImage/{{$offer->foodImg}}" id="showImage" class="img-fluid" alt="{{$offer->foodname}}"></img>
                        </td>
                        <td>{{$offer->description}}</td>
                        <td>{{$offer->typename}}</td>
                        <td>{{$offer->size_name}}</td>
                        <td>{{$offer->orignal_price}}</td>
                        <td>
                         {{$offer->price}}  
                        </td>
                        <td>
                            <a class="btn btn-warning removeoffer" data-id = "{{$offer->id}}">Remove Offer</a>
                        </td>
                        


                    </tr>
                    @endforeach
                </tbody>

            </table>
        </p>

        <!--tap panel content show description-->
    </div>
             
@endsection