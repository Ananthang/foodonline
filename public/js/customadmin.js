function dropper1() {
    $(".dropper1").bind('click', drop1);
}

function dropper2() {
    $(".dropper2").bind('click', drop2);
}


function drop1() {
    $(".dropp1").toggle();
}

function drop2() {
    $(".dropp2").toggle();
}
// $(".button-collapse").sideNav();

function totalsales() {
    var total = 0;
    $('.pricesales').each(function() {
        var price = $(this).data('id');
        total += price;
    });
    $('#totalOrderSale').text(total);
}

function total() {
    var total = 0;

    $('.priceOrder').each(function() {
        var subtotal = 0;
        var id = $(this).data('id');
        var priceId = "#price" + id;
        var quantityId = "#quantity" + id;
        var subtotalId = "#subtotal" + id;
        var quantityval = $(quantityId).data('id');
        var priceval = $(subtotalId).data('id');
        subtotal = priceval * quantityval;
        total += subtotal;
        priceval = 0;
        quantityval = 0;
        $(subtotalId).text(subtotal);
    });



    $("#totalOrder").text(total);
}

$(document).ready(function() {
    $('.dropp2').slideUp();
    total();
    totalsales();


    $('table#TableId').DataTable({
        "info": false,

        //"scrollY": true,	
        //"scrollCollapse": true,
        //fixedHeader: false,
        fixedColumns: true
    });
    $('table#Table1Id').DataTable({
        "info": false,

        //"scrollY": true,	
        //"scrollCollapse": true,
        //fixedHeader: false,
        fixedColumns: true
    });
    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });
    //offer start here





    $(document).on('click', 'a.addoffer', function() {
        var id = $(this).data('id');
        var price_id = "#price" + id;
        var orignal_price = $(price_id).data('id');
        var input_id = "#input" + id;

        var offer_Price = $(input_id).val();

        var url = "/admin/home/offer";


        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                id: id,
                orignal_price: orignal_price,
                offer_price: offer_Price,
            },
            success: function(data) {
                console.log(data);

                $('.offerupdate').load(location.href + " .offerupdate");
                $('.offerupdate1').load(location.href + " .offerupdate1");
                swal({
                    position: 'top-end',
                    type: 'success',
                    title: ' Your Shop Offer Item is Added',
                    showConfirmButton: false,
                    timer: 3000
                })
            },
            error: function(data) {
                console.log(data);
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! please try again',
                    footer: 'Sorry....',
                })

            }
        });

    });
    $(document).on('click', 'a.removeoffer', function() {
        var id = $(this).data('id');
        var url = "/admin/home/offer/delete/" + id;



        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    dataType: 'json',
                    type: 'GET',
                    url: url,
                    data: {
                        id: id
                    },
                    success: function(data) {
                        console.log(data);

                        $('.offerupdate').load(location.href + " .offerupdate");
                        $('.offerupdate1').load(location.href + " .offerupdate1");
                        swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    },
                    error: function(data) {
                        console.log(data);
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong! please try again',
                            footer: 'Sorry....',
                        })

                    }
                });

            } else if (
                // Read more about handling dismissals
                result.dismiss === swal.DismissReason.cancel
            ) {
                swal(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })



    });

    //offers end here

    //Special Start here

    $(document).on('click', 'a.addSpecialItem', function() {
        var id = $(this).data('id');

        var url = "/admin/home/special";


        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                id: id,
            },
            success: function(data) {
                console.log(data);

                $('.specialupdate').load(location.href + " .specialupdate");
                $('.specialupdate1').load(location.href + " .specialupdate1");
                swal({
                    position: 'top-end',
                    type: 'success',
                    title: 'Thank! Your add A Special Item',
                    showConfirmButton: false,
                    timer: 3000
                })
            },
            error: function(data) {
                console.log(data);
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! please try again',
                    footer: 'Sorry....',
                })

            }
        });

    });
    $(document).on('click', 'a.removeSpecialItem', function() {
        var id = $(this).data('id');
        var url = "/admin/home/special/delete/" + id;



        swal({
            title: 'Are you sure?',
            text: "You won't be able to revert this!",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, delete it!',
            cancelButtonText: 'No, cancel!',
            confirmButtonClass: 'btn btn-success',
            cancelButtonClass: 'btn btn-danger',
            buttonsStyling: false,
            reverseButtons: true
        }).then((result) => {
            if (result.value) {

                $.ajax({
                    dataType: 'json',
                    type: 'GET',
                    url: url,
                    data: {
                        id: id
                    },
                    success: function(data) {
                        console.log(data);
                        $('.specialupdate').load(location.href + " .specialupdate");
                        $('.specialupdate1').load(location.href + " .specialupdate1");

                        swal(
                            'Deleted!',
                            'Your file has been deleted.',
                            'success'
                        )
                    },
                    error: function(data) {
                        console.log(data);
                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong! please try again',
                            footer: 'Sorry....',
                        })

                    }
                });

            } else if (
                // Read more about handling dismissals
                result.dismiss === swal.DismissReason.cancel
            ) {
                swal(
                    'Cancelled',
                    'Your imaginary file is safe :)',
                    'error'
                )
            }
        })



    });

    //Special End here


    //admin order update start Here

    $(document).on('click', 'a.orderUpdate', function() {
        var id = $(this).data('id');
        var selectId = "#selectStatus" + id;
        var status = $(selectId).val();
        var url = "/admin/home/order/update";
        var total = 0;

        $('.priceOrder').each(function() {
            var subtotal = 0;
            var id = $(this).data('id');
            var priceId = "#price" + id;
            var quantityId = "#quantity" + id;
            var subtotalId = "#subtotal" + id;
            var quantityval = $(quantityId).data('id');
            var priceval = $(subtotalId).data('id');
            subtotal = priceval * quantityval;
            total += subtotal;
            priceval = 0;
            quantityval = 0;
            $(subtotalId).text(subtotal);
        });


        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                id: id,
                status: status,
                total: total,

            },
            success: function(data) {

                $('.offerupdate').load(location.href + " .addCartList");

                swal({
                    position: 'top-end',
                    type: 'success',
                    title: 'Thank! For Your order is placed you are status Changed',
                    showConfirmButton: false,
                    timer: 3000
                })
            },
            error: function(data) {

                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! please try again',
                    footer: 'Sorry....',
                })

            }
        });

    });



    //admin order update end Here
});

//line

$('.searchShow').mouseover(function() {
    $('#searchBoxId').addClass('show').removeClass('hide');
});
$('.searchShow').mouseleave(function() {
    $('#searchBoxId').addClass('hide').removeClass('show');
});