@extends('layouts.homenav')

@section('title','Shop Profile')

@section('content')
<br>
<br>
<br>


   

        <div class="card card-cascade">

            <p>
             <table class="table table-striped table-bordered table-hover " id="commentTableId">
                <thead class="">
                    <tr class="">
                        <th>Order Id</th>
                        <th>Customer Name</th>
                        <th>Customer Phone Number</th>
                        <th>Order Type</th>
                        <th>Deleivery Type</th>
                        <th>Status</th>
                        <th>Food Items name</th>
                        <th>Food Quantity</th>
                        <th>Food Price</th>
                        <th>Sub Total</th>
                        <th>Update</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)

                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->customername}}</td>
                        <td>
                            {{$order->phoneno}}
                        </td>
                        <td>{{$order->order_type}}</td>
                        <td>{{$order->delivery_type}}</td>
                    <td>
                        <select class="custom-select" aria-placeholder="{{ $order->status }}" id = "selectStatus{{$order->id}}">
                           
                            <option value="Order Pending">Order Pending</option>
                            <option value="Accepted And Preparing">Accepted And Preparing</option>
                            <option value="Ready To Deleivery">Ready To Deleivery</option>
                            <option value="Delivered">Delivered</option>
                        </select>
                        </td>
                        <td>{{$order->foodname}}</td>
                    <td id="quantity{{ $order->id }}" data-id="{{$order->quantity}}">{{$order->quantity}}</td>
                        <td class="priceOrder" id="price{{ $order->id }}" data-id = "{{ $order->id }}">{{$order->price}}</td>
                        <td id ="subtotal{{ $order->id }}" data-id="{{$order->price}}"></td>
                        <td>
                             <a class="btn btn-warning orderUpdate" data-id = "{{$order->id}}">Update</a>
                        </td>
                        


                    </tr>
                    @endforeach
                </tbody>

            </table>
        </p>
        <table class="table table-striped table-bordered table-hover">
          <thead class="">
            <tr class="">
              <th>
                <strong>Total (Rs.)</strong>
              </th>
              <th id="totalOrder"></th>

            </tr>
          </thead>

        </table>
        </div>
        
   
<!--/main body page-->
@endsection