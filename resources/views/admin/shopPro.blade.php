@extends('layouts.homenav')

@section('title','Shop Profile')

@section('content')
<br>
<br>
<br>

<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="card card-cascade">



            <div class="view overlay hm-white-slight">
                <img src="/storage/upload/shoplogo/{{ $shops->shoplogo }}" id="profileImg" class="img-fluid" alt="">
                <a class="waves-effect waves-light"></a>
                <div class="mask"></div>
            </div>

            <h3>Profile</h3>
            <div class="card-body">
                <label class="thicker" for="">
                    <strong>Shop Name:-</strong>
                </label>
                <p id="tab1">{{ $shops->shopname }}</p>
                <br>

                <label class="thicker" for="">
                    <strong>Shop Address:-</strong>
                </label>
                <p id="tab1">{{ $shops->address }}</p>
                <br>

                <label class="thicker" for="">
                    <strong>Email Address:-</strong>
                </label>
                <p id="tab1">{{ $shops->email }}</p>
                <br>

                <label class="thicker" for="">
                    <strong>Shop Description:-</strong>
                </label>
                <p id="tab1">{{ $shops->shop_description }}</p>
                <br>

                <label class="thicker" for="">
                    <strong>Shop Contact No:-</strong>
                </label>
                <p id="tab1">{{ $shops->phoneno }}</p>
                <br>

               
                 
                
                <a class="btn btn-primary" data-toggle="modal" data-target="#profileEdit">Edit Profile</a>
            </div>

        </div>
        <div id="profileEdit" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                     <well><div class="modal-header well">
                      <h4 class="modal-title">Update Shop Details</h4>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <br/>
                    </div></well> 
                    <div class="modal-body">
                        <div class="form-group">
                        <form action="/anan/{{session()->get('shop_id')}}" enctype="multipart/form-data" method="POST">
                                {{csrf_field() }}
                               {{method_field('PUT')}}
                               
                        <input type="hidden" name="id" value="{{session()->get('shop_id')}}">

                                <i class="fa fa-shopping-basket  prefix grey-text"></i>
                                <label for="orangeForm-email">Shopname</label>
                                <div class="form-group">
                                    <input type="text" id="orangeForm-name" name="shopname" value="{{old('shopname')}}" class="form-control">
                                </div>


                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="orangeForm-address">Address</label>
                                <div class="form-group">
                                    <input type="text" id="orangeForm-address" name="address" value="{{old('address')}}" class="form-control">
                                </div>

                                <i class="fa fa-envelope prefix grey-text"></i>
                                <label for="orangeForm-name">Email Address</label>
                                <div class="form-group">
                                    <input type="text" id="orangeForm-email" name="email" value="{{old('email')}}" type="email" class="form-control">
                                </div>


                                <i class="fa fa-phone prefix grey-text"></i>
                                <label for="orangeForm-phoneno">Phone Number</label>
                                <div class="form-group">
                                    <input type="text" id="orangeForm-phoneno" name="phoneno" value="{{old('phoneno')}}" class="form-control">
                                </div>

                                
                                <i class="fa fa-phone prefix grey-text"></i>
                                <label for="orangeForm-shop_description">Shop Description</label>
                                <div class="form-group">
                                    <textarea type="textarea" id="orangeForm-shop_description" name="shop_description" value="{{old('shop_description')}}" class="form-control"></textarea>
                                </div>


                                <i class="fa fa-picture-o prefix "></i>
                                <label for="orangeForm-shoplogo"></label>
                                <div class="form-group">
                                    <input type="file" id="orangeForm-shoplogo" name="shoplogo"  class="form-control btn-success">
                                </div>

                               <div class=" form-group {{ $errors->has('username') ? ' has-error' : '' }}">
                                    <i class="fa fa-key prefix grey-text"></i>
                                    <label for="orangeForm-username" class="col-md-4 control-label">username</label>
                                    <input type = "username" class="form-control" name="username"  required >
                                </div>

                               <div class=" form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <i class="fa fa-key prefix grey-text"></i>
                                    <label for="orangeForm-password" class="col-md-4 control-label">Password</label>
                                    <input type = "password" class="form-control" name="password"  required >
                                </div>


                                <div class="text-center">
                                    <button class="btn btn-primary" name="submit" type="submit">Update Account</button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3"></div>

</div>
<!--/main body page-->
@endsection