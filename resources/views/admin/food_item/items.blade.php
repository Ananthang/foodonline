@extends('layouts.homenav')

@section('title','Food Items')

@section('content')

<br>
<br>
<br> @if(session()->has('message'))
<p class="alert alert-success"> {{session()->get('message')}} </p>

@endif @if(session()->has('messageDele'))

<p class="alert alert-danger"> {{session()->get('messageDele')}} </p>
@endif
<ul class="nav nav-tabs nav-justified blue" role="tablist">
    <li class="nav-item ">
        <a class="nav-link active" data-toggle="tab" href="#addNewItem" role="tab">
            Add New Item
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" data-toggle="tab" href="#showItem" role="tab">
            Show All Items
        </a>
    </li>
</ul>
<!-- /tab panel list-->
<!--tap panel content -->
<div class="tab-content card">
    <!--tap panel content show description-->
    <div class="tab-pane fade  show active" id="addNewItem" role="tabpanel">
        <br>
        <p>
            <div class="card card cascade form-group">
                <div class="view overlay hm-white-slight form-group">

                    <form action="/admin/home/fooditem" enctype="multipart/form-data" method="POST" id="formId">
                        {{csrf_field()}}


                            <i class="fa fa-food prefix grey-text"></i>
                            <label for="orangeForm-foodname">Food Name</label>
                        <div class="form-group">
                            <input type="text" id="orangeForm-foodname" name="foodname" class="form-control">
                        </div>

                            <i class="fa fa-food prefix grey-text"></i>
                        <label for="orangeForm-foodImg">Image</label>
                        <div class="form-group">
                            <input type="file" id="orangeForm-foodImg" name="foodImg" class="form-control btn btn-primary">
                            <br>
                        </div>

                        <br>


                        <i class="fa fa-food prefix grey-text"></i>
                        <label for="orangeForm-description">Description</label>
                        <div class="form-group">
                            <textarea type="text" id="orangeForm-description" name="description" class="form-control"></textarea>
                        </div>


                        <div class="form-group">
                            <label for="sel1">Food Type</label>
                            <select class="form-control" name="food_types_id" id="sel1">
                                @foreach ($foodtypes as $foodtype)
                                <option value="{{$foodtype->id}}">{{$foodtype->typename}}</option>
                                @endforeach
                            </select>
                        </div>


                      
                        <label for="orangeForm-sizename">Size</label>
                        <div class="form-group">
                            
                            <input type="text" id="orangeForm-sizename" name="sizename" class="form-control" onclick="toastr.info('Hi! If you have more than one size than please fill the size1 and 2 prices also orderly. ');">
                            
                            <small>This for a size of the food If you have more than one size and price please fill it.</small>
                        </div>

                        <i class="fa fa-money prefix grey-text"></i>
                        <label for="orangeForm-price">Price</label>
                        <div class="form-group">

                            <input type="text" id="orangeForm-price" name="price" class="form-control">

                        </div>

                        
                        <label for="orangeForm-sizename1">Another Size</label>
                        <div class="form-group">
                            <input type="text" id="orangeForm-sizename1" name="sizename1" class="form-control">
                        </div>

                        <i class="fa fa-money prefix grey-text"></i>
                        <label for="orangeForm-price1">Price</label>
                        <div class="form-group">
                            <input type="text" id="orangeForm-price1" name="price1" class="form-control">
                        </div>

                      
                        
                        <label for="orangeForm-sizename2">Another Size</label>
                        <div class="form-group">
                            <input type="text" id="orangeForm-sizename2" name="sizename2" class="form-control">
                        </div>

                        <i class="fa fa-money prefix grey-text"></i>
                        <label for="orangeForm-price2">Price</label>
                        <div class="form-group">
                            <input type="text" id="orangeForm-price2" name="price2" class="form-control">
                        </div>




                        <center><button type="submit" class="btn btn-outline-success btn-lg">submit</button>
                       
                       <button type="reset" class="btn btn-outline-danger btn-lg">cancel</button></center>
                       
                    </form>
                </div>
            </div>
        </p>
        @if ($errors->any()) @foreach ($errors->all() as $error)
        <div class="alert alert-danger">
            {{ $error }}
        </div>
        @endforeach @endif
    </div>

    <div class="tab-pane fade" id="showItem" role="tabpanel">
        <br>
        <p>
            <table class="table table-striped table-bordered table-hover " id="TableId">
                <thead class="">
                    <tr class="">
                        <th>Food Item id</th>
                        <th>Food Name</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Size</th>
                        <th>Price</th>
                        <th>Update</th>
                        <th>Delete</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($foods as $food)

                    <tr>
                        <td>{{$food->id}}</td>
                        <td>{{$food->foodname}}</td>
                        <td>
                            <img src="/storage/upload/foodImage/{{$food->foodImg}}" id="showImage" class="img-fluid" alt="{{$food->foodname}}"></img>
                        </td>
                        <td>{{$food->description}}</td>
                        <td>{{$food->typename}}</td>
                        <td>{{$food->size_name}}</td>
                        <td>{{$food->price}}</td>
                        <td>
                            <a class="btn btn-warning" href="/admin/home/fooditem/{{$food->id}}/edit">Update</a>
                        </td>
                        <td>
                            <form action="/admin/home/fooditem/{{$food->id}}" method="post" enctype="multipart/form-data" class="form-group">
                                <input type="hidden" name="id" value="{{$food->id}}"> {{ csrf_field() }} {{method_field('DELETE')}}

                                <button class="btn btn-danger" type="submit" name="submit">Delete</button>
                            </form>
                        </td>


                    </tr>
                    @endforeach
                </tbody>

            </table>
        </p>

        <!--tap panel content show description-->
    </div>

    <!-- Nav tabs -->

    <!--/.Panel 3-->

    @endsection