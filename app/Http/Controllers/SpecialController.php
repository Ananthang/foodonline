<?php

namespace App\Http\Controllers;

use App\Special;
use App\Auth;
use App\User;
use App\FoodType;
use App\Shop;
use App\FoodItem;
use App\FoodSize;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;

class SpecialController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('shop_id')){

                  $shop_id = session('shop_id');
          $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
            'specials'   =>  DB::table('food_items')
            
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('specials','food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
                'shops' =>Shop::find(session()->get('shop_id')),  
);
        return view ('admin.special')->with($data);
    }
       else{
           return Redirect::to('/admin');
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        

        $food_sizes_id = $request->id;
        $shop_id = session()->get('shop_id');
        $special = new special;
        $special->shop_id = $shop_id;
        $special->food_sizes_id = $food_sizes_id;
        $special->save();


                $shop_id = session('shop_id');
          $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),

            'specials'   =>  DB::table('food_items')
            
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('specials','food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','specials.id')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
                'shops' =>Shop::find(session()->get('shop_id')),  
);
return $data;


    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Special  $special
     * @return \Illuminate\Http\Response
     */
    public function show(Special $special)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Special  $special
     * @return \Illuminate\Http\Response
     */
    public function edit(Special $special)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Special  $special
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Special $special)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Special  $special
     * @return \Illuminate\Http\Response
     */
    public function destroy(Special $special)
    {
        
    }

    public function destroyItem($id){
       

        $special = Special::find($id);
        $special->delete();

           $shop_id = session('shop_id');
          $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),

            'specials'   =>  DB::table('food_items')
            
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('specials','food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','specials.id')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
                'shops' =>Shop::find(session()->get('shop_id')),  
);
return $data;

       
    }
}
