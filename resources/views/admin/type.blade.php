@extends('layouts.homenav')

@section('title','Food Types')

@section('content')
<br>
<br>

<main>

    <!--main body page-->


    <div class="row">

        <div class="col-md-1">

        </div>
        <div class="col-md-10">
            <br>
            <br>
            <br>
            <div class="panel-body">
                @if (session('type'))
                <p class="alert alert-success">
                    {{ session()->get(' type') }}

                </p>
                @endif
            </div>
            <ul class="nav nav-tabs nav-justified blue" role="tablist">
                <li class="nav-item ">
                    <a class="nav-link active" data-toggle="tab" href="#addNewItem" role="tab">
                        Add New Item
                    </a>
                </li>
                <li class="nav-item ">
                    <a class="nav-link" data-toggle="tab" href="#showItem" role="tab">
                        Show All Items
                    </a>
                </li>
            </ul>
            <!-- /tab panel list-->
            <!--tap panel content -->
            <div class="tab-content card">
                <!--tap panel content show description-->
                <div class="tab-pane fade  show active" id="addNewItem" role="tabpanel">
                    <br>
                    <p>
                        <div class="card card cascade">
                            <div class="view overlay hm-white-slight">
                                
                                <form action="/admin/home/type" method="POST" id="formId">
                                    {{csrf_field()}}
                                    <input name='shop_id' type="hidden" value="{{session('shop_id')}}">


                                    <i class="fa fa-food prefix grey-text"></i>
                                    <label for="orangeForm-typename">Food Type Name</label>
                                    <div class="form-group">

                                        <input type="text" id="orangeForm-typename" name="typename" class="form-control">

                                    </div>


                                    <button type="submit" class="btn btn-success">submit</button>
                                </form>
                            </div>
                        </div>
                    </p>

                </div>

                <div class="tab-pane fade" id="showItem" role="tabpanel">
                    <br>
                    <p> 
                        <table class="table table-striped table-bordered table-hover ">
                            <thead class="mdb-color darken-3">
                                <tr class="">
                                    <th>Food Type Id</th>
                                    <th>Food Type Name</th>
                                    <th>Update</th>
                                    <th>Delete</th>
                                </tr>
                            </thead>
                            <tbody>
                               
                 @foreach($foodtypes as $foodtype)

                    <tr>
                        <td>{{$foodtype->id}}</td>
                        <td>{{$foodtype->typename}}</td>
                        
                        
                        
                        <td>
                            <a class="btn btn-warning" href="/admin/home/type/{{$foodtype->id}}/edit">Update</a>
                        </td>
                        <td>
                            <form action="/admin/home/type/{{$foodtype->id}}" method="post" enctype="multipart/form-data" class="form-group">
                                <input type="hidden" name="id" value=""> {{ csrf_field() }} {{method_field('DELETE')}}

                                <button class="btn btn-danger" type="submit" name="submit">Delete</button>
                            </form>
                        </td>


                    </tr>
                    @endforeach
                   
                            </tbody>

                        </table>
                     
                    </p>
 
                    <!--tap panel content show description-->
                </div>

            </div>

            <div class="col-md-1">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br> @if ($errors->any()) @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
                @endforeach @endif
            </div>
        </div>

        <!--/main body page-->
    </div>
    </div>
</main>



@endsection