@extends('layouts.homenav')

@section('title','Food Items Edit' )

@section('content') @if(session()->has('message'))
<p class="alert alert-success"> {{session()->get('message')}} </p>

@endif

<br>
<br>
<br>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="card card-cascade">


            <div class="card-body">
            <form action="/admin/home/type/{{$foodtypes->id}}" method="POST" enctype="multipart/form-data" id="formReg">
                    {{csrf_field() }} {{method_field('PUT')}}
                    <p class="h3 text-center mb-4 thicker">Update Food Items</p>
            <input type = "hidden" name = "shop_id" value ="{{$foodtypes->shop_id}}"
                    <input type="hidden" name="id" value="{{$foodtypes->id}}">
                    <label for="orangeForm-foodname"> Food Type Name</label>
                    <div class="form-group">
                        <input type="text" id="orangeForm-food_type_name" name="typename" class="form-control">
                    </div>


                    <div class="text-center">
                        <button class="btn btn-outline-primary" name="submit" type="submit">Update Items</button>
                        <button type="reset" class="btn btn-outline-danger btn-lg">cancel</button>
                    </div>
                </form>

            </div>

        </div>

    </div>


    <div class="col-md-3"></div>

</div>
<!--/main body page-->


@endsection