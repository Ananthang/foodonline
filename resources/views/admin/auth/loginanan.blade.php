<!DOCTYPE html>




<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shop User Login</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/font.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">


    <!-- Your custom styles (optional) -->
    <link href="/css/stylelogin.css" rel="stylesheet">
</head>

<body>

    <!-- Start your project <here-->
    <main>
        <div class="container">
            <br>
            <br>
            <br>
            <br>

            <div class="row">
                <div class="col-md-3 col-sm-3 col-lg-3"></div>
                <div class="col-md-6 col-sm-6 col-lg-6">
                    <center>

                        <br>
                    </center>
                    <div id="form">
                        <div class="card wow card-cascade transparent " id="loginImg" data-wow-delay="0.3s"> 
                            <div class="card-header" id="hea">

                                            <span class="text-center"><h3>Login Form</h3></span>

                                    </div>
                            <div class="card-body">
                                <form class="form-horizontal" id="formReg" method="POST" action="/register">
                                    {{ csrf_field() }}
                                    <br>
                                    <br>
                                    <br>
                                   
                                    <div class="form-group {{$errors->has('Username') ? ' has-error' : ''}}">
                                        <label for="username" class="col-md-4 col-sm-4 col-lg-4 control-label">
                                            <i class="fa fa-user prefix grey-text"></i>
                                            <span> Username</span>
                                        </label>

                                        <div class="form-group">
                                            <input id="username" type="text" class="form-control" name="username" value="{{old('username')}}" placeholder="Username"
                                                required autofocus> @if ($errors->has('Username'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('Username')}}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                        <label for="password" class="col-md-4 col-sm-4 col-lg-4 control-label">
                                            <i class="fa fa-key prefix grey-text"></i>
                                            <span>Password</span>
                                        </label>

                                        <div class="form-group">
                                            <input id="password" type="password" class="form-control" placeholder="Password" name="password" required> @if ($errors->has('password'))
                                            <span class="help-block">
                                                <strong>{{$errors->first('password')}}</strong>
                                            </span>
                                            @endif
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <div class="col-md-6 col-sm-6 col-lg-6 col-md-offset-4">
                                            <div class="checkbox">
                                                <label>
                                                    <input type="checkbox" name="remember" {{ old( 'remember') ? 'checked' : '' }}> Remember Me
                                                </label>
                                            </div>
                                        </div>
                                    </div>
<br>


                                    <div class="form-group">
                                        <div class="col-md-8 col-sm-8 col-lg-8 col-md-offset-4">
                                            <button type="submit" class="btn btn-primary btn-lgs">
                                                Login
                                            </button>

                                            
                                        </div>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-3 col-sm-3 col-lg-3">
                @if ($errors->any()) @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
                @endforeach @endif
            </div>
        </div>


    </main>
    </here-->
    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->

</body>

</html>