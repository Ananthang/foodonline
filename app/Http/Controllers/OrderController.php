<?php

namespace App\Http\Controllers;

use App\Order;
use App\OrderTem;
use App\Order_items;
use App\Customer;
use App\User;
use App\FoodType;
use App\Shop;
use App\FoodItem;
use App\FoodSize;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Notifications\Notifiable;
use App\Notifications\TaskCustomerOrder;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;

class OrderController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
         $data = array(
    'foodtypes'  =>  FoodType::all(),
    'foods'   =>  DB::table('food_items')->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')->join('food_types' , 'food_items.food_types_id','=','food_types.id')->select('food_items.id','food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price')->get(),
    'shops' =>Shop::find(session()->get('shop_id')),
    
);
       
        return $data;
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
    $order_type = $request->order_type;
    $code_id = $request->code_id;
    $email = $request->emailaddress;
    $phoneno = $request->phoneno;
    $address = $request->address;
    $city = $request->city;
    $orderdate = $request->orderdate;
    $delivery_type = $request->delivery_type;
       
        $customername = $request->name;


         
     if($request->has('address')){
        
        $orderTs = DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('order_tems','order_tems.food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','order_tems.quantity','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where([['food_types.shop_id','=',session()->get('shop_id')],['order_tems.code_id','=',session()->get('code_id')]])
            ->get();

            $customerps = Customer::where([['shop_id' ,'=',session()->get('shop_id')],['phoneno' ,'=',$request->phoneno]])->get();
            if($customerps->isNotEmpty()){
             $customers = Customer::where([['shop_id' ,'=',session()->get('shop_id')],['phoneno' ,'=',$request->phoneno],['address' , '=',$address]])->get();
                if($customers->isNotEmpty()){
                    foreach($customers as $customer1){
                        $customer_id = $customer1->id;
                    }
                   
                    $order = new Order ; 
                    $order->customer_id = $customer_id;
                    $order->status = "pending";
                    $order->order_date_time = $orderdate;
                    $order->delivery_type = $delivery_type;
                    $order->order_type = $order_type;
                    if ($order->save()){
                        $ordernews = Order::where('customer_id','=',$customer_id)->latest()->get();
                        foreach($orderTs as $orderT){
                            $order_items = new order_items;
                            $order_items->food_sizes_id = $orderT->id;
                            $order_items->quantity = $orderT->quantity;
                            foreach($ordernews as $ordernew){
                                 $order_items->order_id = $ordernew->id;
                           }
                            $order_items->save();
                        }
                    }
                    
                }
                else{//update customer address
                    foreach($customerps as $customer1){
                        $customer_id = $customer1->id;
                    }
                     
                        $customer = customer::find($customer_id);
                        $customer->email = $email;
                        $customer->phoneno = $phoneno;
                        $customer->address = $address;
                        $customer->city = $city;
                        $customer->customername = $customername;
                        $customer->shop_id = session()->get('shop_id');
                        $customer->save();
                       
                        $order = new Order ; 
                    $order->customer_id = $customer_id;
                    $order->status = "pending";
                    $order->order_date_time = $orderdate;
                    $order->order_type = $order_type;
                    $order->delivery_type = $delivery_type;
                    if ($order->save()){
                        $ordernews = Order::where('customer_id','=',$customer_id)->get();
                        foreach($orderTs as $orderT){
                            $order_items = new order_items;
                            $order_items->food_sizes_id = $orderT->id;
                            $order_items->quantity = $orderT->quantity;
                             foreach($ordernews as $ordernew){
                                 $order_items->order_id = $ordernew->id;
                           }
                            $order_items->save();
                        }
                    }
                    
                }
            }
            else{
                $customer = new Customer;
                        $customer->email = $email;
                        $customer->phoneno = $phoneno;
                        $customer->address = $address;
                        $customer->city = $city;
                        $customer->customername = $customername;
                        $customer->shop_id = session()->get('shop_id');
                        $customer->save();
                        $customers = customer::where('phoneno','=',$phoneno)->get();
                        $order = new Order ; 
                        foreach( $customers as $customer){
                           $customer_id = $customer->id;
                        }
                    $order->customer_id = $customer_id;
                    $order->status = "pending";
                    $order->order_date_time = $orderdate;
                    $order->order_type = $order_type;
                    $order->delivery_type = $delivery_type;
                    if ($order->save()){
                        $ordernews = Order::where('customer_id','=',$customer_id)->latest()->get();
                        foreach($orderTs as $orderT){
                            $order_items = new order_items;
                            $order_items->food_sizes_id = $orderT->id;
                            $order_items->quantity = $orderT->quantity;
                             foreach($ordernews as $ordernew){
                                 $order_items->order_id = $ordernew->id;
                           }
                            $order_items->save();
                        }
                    }

            }

        }
        else{
        
        $orderTs = DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('order_tems','order_tems.food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','order_tems.quantity','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where([['food_types.shop_id','=',session()->get('shop_id')],['order_tems.code_id','=',session()->get('code_id')]])
            ->get();

            $customers = Customer::where([['shop_id' ,'=',session()->get('shop_id')],['phoneno' ,'=',$request->phoneno]])->get();
            if($customers->isNotEmpty()){
                $flag = false;
               foreach($customers as $customer){
                    $customer_id = $customer->id;
                      if($customer->address == "defalut"){
                          $flag == true;
                          
                      }
                  }
              if($flag){
                  foreach($customers as $customer){
                      $customer_id = $customer->id;
                  }
                    
                    $order = new Order ; 
                    $order->customer_id = $customer_id;
                    $order->status = "pending";
                    $order->order_date_time = $orderdate;
                    $order->order_type = $order_type;
                    $order->delivery_type = $delivery_type;
                    if ($order->save()){
                        $ordernews = Order::where('customer_id','=',$customer_id)->latest()->get();
                        foreach($orderTs as $orderT){
                            $order_items = new order_items;
                            $order_items->food_sizes_id = $orderT->id;
                            $order_items->quantity = $orderT->quantity;
                             foreach($ordernews as $ordernew){
                                 $order_items->order_id = $ordernew->id;
                           }
                            $order_items->save();
                        }
                    }
                }
                    else{
                   
                    $customer = customer::find($customer_id);
                        $customer->email = $email;
                        $customer->phoneno = $phoneno;
                        $customer->address = "default";
                        $customer->city = "default";
                         $customer->customername = $customername;
                        $customer->shop_id = session()->get('shop_id');
                        $customer->save(); 
                        $order = new Order ; 
                    $order->customer_id = $customer_id;
                    
                    $order->customer_id = $customer_id;
                    $order->status = "pending";
                    $order->order_date_time = $orderdate;
                    $order->order_type = $order_type;
                    $order->delivery_type = $delivery_type;
                    if ($order->save()){
                        $ordernews = Order::where('customer_id','=',$customer_id)->latest()->get();
                        foreach($orderTs as $orderT){
                            $order_items = new order_items;
                            $order_items->food_sizes_id = $orderT->id;
                            $order_items->quantity = $orderT->quantity;
                             foreach($ordernews as $ordernew){
                                 $order_items->order_id = $ordernew->id;
                           }
                            $order_items->save();
                        }
                    }
                }
                
               }
               else{
                    $customer =new customer;
                        $customer->email = $email;
                        $customer->phoneno = $phoneno;
                        $customer->address = "default";
                        $customer->city = "default";
                         $customer->customername = $customername;
                        $customer->shop_id = session()->get('shop_id');
                        $customer->save(); 
                        $customers = customer::where('phoneno','=',$phoneno)->get();
                        $order = new Order ; 
                        foreach($customers as $customer){
                            $customer_id = $customer->id;
                        }
                    $order->customer_id = $customer_id;
                    
                    $order->customer_id = $customer_id;
                    $order->status = "pending";
                    $order->order_date_time = $orderdate;
                    $order->order_type = $order_type;
                    $order->delivery_type = $delivery_type;
                    if ($order->save()){
                        $ordernews = Order::where('customer_id','=',$customer_id)->latest()->get();
                        foreach($orderTs as $orderT){
                            $order_items = new order_items;
                            $order_items->food_sizes_id = $orderT->id;
                            $order_items->quantity = $orderT->quantity;
                            foreach($ordernews as $ordernew){
                                 $order_items->order_id = $ordernew->id;
                           }
                            $order_items->save();
                        }
                    }
               }
            }

            $orderTs = DB::table('order_tems')
            ->where('code_id','=',session()->get('code_id'))
            ->delete();

           
          $data = array(
    'foodtypes'  =>  FoodType::all(),
    'foods'   =>  DB::table('food_items')->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')->join('food_types' , 'food_items.food_types_id','=','food_types.id')->select('food_items.id','food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price')->get(),
    'shops' =>Shop::find(session()->get('shop_id')),
    'customer' =>customer::find($customer_id)->get(),
    'order' =>order::where('customer_id','=',$customer_id)->latest()->get(),
    'order_items'=>order_items::where('order_id','=',$ordernew->id),
    
);
    
    Customer::find($customer_id)->notify(new TaskCustomerOrder);

    
            return $data;



    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function show(Order $order)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function edit(Order $order)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Order $order)
    {
        
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Order  $order
     * @return \Illuminate\Http\Response
     */
    public function destroy(Order $order)
    {
        //
    }
}
