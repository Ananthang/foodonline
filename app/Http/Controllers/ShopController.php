<?php

namespace App\Http\Controllers;

use App\Shop;
use Illuminate\Http\Request;
use Image;
use App\User;
use Hash;
use File;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;

class ShopController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {

if (session()->has('anan')){
         return view('admin.shopRegi');
        }else{
             return Redirect::to('/register');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        
    }
    

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
if (session()->has('anan')){

        $validator = Validator::make($request->all(), [
            'shoplogo' =>'required',
            'shopname' =>'required|string|max:255',  
            'address'  =>'required|string|max:255', 
            'shop_description'  =>'required|string|max:255', 
            'email'    =>'required|email|max:255',
            'phoneno'  =>'required|numeric',
        ]);


        if($validator->fails()){
             return redirect('/anan')
                        ->withErrors($validator)
                        ->withInput();
        }
        else {
             $shoplogo = $request->file('shoplogo');
            $shopname = $request->shopname;
            $address = $request->address;
            $shop_description = $request->shop_description;
            $email= $request->email;
            $phoneno= $request->phoneno;
              
            $filename = $shopname.'.'.$shoplogo->getClientOriginalExtension();
            $request->shoplogo->storeAS('public/upload/shoplogo',$filename);
            $shop = new Shop;
            $shop->shoplogo = $filename;
            $shop->shopname = $shopname;
            $shop->address = $address;
            $shop->shop_description = $shop_description;
            $shop->email = $email;
            $shop->phoneno = $phoneno;
    
            if($shop->save()){
                
               session()->flash('shop', 'your Shop is created . please insert your owner details here.' );
                      
            $users = DB::table('shops')
                            ->where('email', '=', $email)
                            ->select('id')
                            ->latest()
                            ->get();
                            foreach($users as $user){
                                Session::flash('id', $user->id);
                            }
                             
              return  redirect('/reg');
           
            }

            Session::flash('shop', 'your Shop is not created . please try again laters');
           
            return view('admin.ShopRegi');
        }
    }
    else{
             return Redirect::to('/register');
        }
    }


    /**
     * Display the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function show(Shop $shop)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {

        $shops = Shop::find(Auth::user()->shop_id);
        $users = User::find(Auth::user()->shop_id);
        return view('admin.ShopPro',compact('shops'),compact('users'));
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request)
    {
         $id = $request->id;
        $shop = Shop::find($id);
        $shoplogo = $request->file('shoplogo');
            $shopname = $request->shopname;
            $address = $request->address;
            $shop_description = $request->shop_description;
            $email= $request->email;
            $phoneno= $request->phoneno;
           
              
            $filename = $shopname.'.'.$shoplogo->getClientOriginalExtension();
            $request->shoplogo->storeAS('public/upload/shoplogo',$filename);
            
           // $shop->shoplogo = $filename;
           // $shop->shopname = $shopname;
           // $shop->address = $address;
          //  $shop->email = $email;
          //  $shop->phoneno = $phoneno;
        

           $userdata = array(
        'username'  => $request->username,
        'password'  => $request->password
    );
    

    // attempt to do the login
    if (Auth::attempt($userdata) ){

    
       
            $shop->shoplogo = $filename;
            $shop->shopname = $shopname;
            $shop->address = $address;
            $shop->shop_description = $shop_description;
            $shop->email = $email;
            $shop->phoneno = $phoneno;
    
            if($shop->save()){
                
               session()->flash('shop', 'your Shop is created . please insert your owner details here.' );
                      
            $users = DB::table('shops')
                            ->where('email', '=', $email)
                            ->select('id')
                            ->latest()
                            ->get();
                            foreach($users as $user){
                                Session::flash('id', $user->id);
                            }
                             
              return  redirect('admin');
            }

            Session::flash('shop', 'your Shop is not created . please try again laters');
           
            return Redirect::to('/admin');
       

    } else {        

        // validation not successful, send back to form 
        return Redirect::to('/admin');

    }




       

    
    }
    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Shop  $shop
     * @return \Illuminate\Http\Response
     */
    public function destroy(Shop $shop)
    {
        //
    }
}
