@extends('layouts.user') @section('title','Home') @section('content')
<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  js.src = 'https://connect.facebook.net/en_GB/sdk.js#xfbml=1&version=v2.12';
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>
<br>
<br>
<br>




<div class="slider" id="slider1">
  <!-- Slides -->
  <div id="carouselImg" style="background-image:url(/storage/upload/foodImage/Thosai1.jpg)"></div>
  <div id="carouselImg" style="background-image:url(/storage/upload/foodImage/pizza.jpg)"></div>
  <div id="carouselImg" style="background-image:url(/storage/upload/foodImage/Hamburger1.jpg)"></div>
  <div id="carouselImg" style="background-image:url(/storage/upload/foodImage/ideli.jpg)"></div>
  <div id="carouselImg" style="background-image:url(/storage/upload/foodImage/rice.jpg)"></div>
  <!-- The Arrows -->
  <i class="left" class="arrows" style="z-index:2; position:absolute;">
    <svg viewBox="0 0 100 100">
      <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z"></path>
    </svg>
  </i>
  <i class="right" class="arrows" style="z-index:2; position:absolute;">
    <svg viewBox="0 0 100 100">
      <path d="M 10,50 L 60,100 L 70,90 L 30,50  L 70,10 L 60,0 Z" transform="translate(100, 100) rotate(180) "></path>
    </svg>
  </i>
  <!-- Title Bar -->

</div>

<!--Add Card Model Start Here.........-->
@if(!empty($orderTs))
<div id="addcartlist" class="modal fade " role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Carts</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <br/>

      </div>
      <div class="modal-body ">


        <div class="alert alert-info"><small>If your want to edit quantity enter quantity and please click update button </small></div>
        <table class="table table-striped table-bordered table-hover addCartList">
          <thead class="">
            <tr class="">
              <th>Food Name</th>
              <th>Image</th>
              <th>Size</th>
              <th class="quantityTitle">Qty</th>
              <th>Price (Rs.)</th>
              <th>Update</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            @foreach($orderTs as $orderT)

            <tr>
              <td>{{ $orderT->foodname }}</td>
              <td>
                <img src="/storage/upload/foodImage/{{$orderT->foodImg}}" id="showImage" class="img-fluid" alt="{{$orderT->foodname}}"></img>
              </td>

              <td>{{ $orderT->size_name }}</td>
              <td>
                <input type="text" value="{{$orderT->quantity}}" placeholder="Qty" id="quantity{{$orderT->id}}" data-id="{{$orderT->id}}"
                  class="form-control quantity">
              </td>
              <td id="price{{$orderT->id}}" data-id="{{ $orderT->price}}" data-last-value="{{ $orderT->id }}" class="orderTPrice">
                <?php 
           $mut  = (int) $orderT->price*(int)$orderT->quantity
             ?>{{$mut}}</td>
              <td>

                <a id="updateAddId{{ $orderT->id }}" data-id="{{ $orderT->id }}" class="updateAddCart">
                  <img id="modalImage" src="/img/user/Text-Edit-icon.png">
                </a id="">

              </td>
              <td>
                <a id="deleteAddId{{ $orderT->id }}" data-id="{{ $orderT->id }}" class="deleteAddCart">
                  <img id="modalImage" src="/img/user/delete-icon.png">
                </a>
              </td>


            </tr>
            @endforeach
          </tbody>

        </table>
        
      </div>
      <div class="modal-footer">
        <a onclick="checkOut()" class="btn btn-success" data-dismiss="modal">Check out </a>
      </div>
    </div>

  </div>
</div>
@endif

<!--Add Card Model Ends Here.........-->

<!--Food Items Offer Start Here ....................-->

@if(!$offers->isEmpty())

<div  id="addcartOfferDiv">
     
      <div class="card text-center welcomeCard" >
        <div class="card-header">
          <h2>
            <strong style="color:red"> Offers </strong>
          </h2>
        </div><?php 
            $count = $offers->count();
            if($count == 1){
              $col_sm = 12 ;
              $col_md = 12 ;
              $col_lg = 12 ;
            }elseif ($count == 2) {
              $col_sm = 12 ;
              $col_md = 5 ;
              $col_lg = 5 ;
            }elseif($count == 3){
              $col_sm = 12 ;
              $col_md = 4 ;
              $col_lg = 4 ;
            }else{
              $col_sm = 3 ;
              $col_md = 12 ;
              $col_lg = 3 ;
            }?>
        <br> 
       
        <div class="card-block" >

          <div class="row"> @foreach($offers as $offer)
            <?php $i = 0;?> @foreach($foods as $food) @if($food->id == $offer->id) @if($i>=4) @break; @endif
            <div class="col-md-{{$col_md}} col-sm-{{$col_sm}} col-lg-{{$col_lg}}">
              <div class="card" id="cardInfo{{ $food->id }}">
                <div class="card-block">

                  <img class="card-img-top" src="/storage/upload/foodImage/{{$food->foodImg}}" class="img-responsive" id="Imagecardfor" alt="Card image cap">
                  <br>
                  <h4 class="card-title">{{ $food->foodname }}</h4>
                  <div class = "back" id="back{{ $food->id }}">
                    <p class="card-text ">{{ $food->description }}</p>
                    <br>
                    <br>
                  </div>
                  <div id="front{{ $food->id }}">
                    <span class="badge badge-success">
                      <p id="cardSize{{ $food->id }}">{{ $food->size_name }}</p>
                    </span>
                    <p>RS.
                      <span id="cardPrice{{ $food->id }}" data-id="{{ $food->price }}">{{ $food->price }}
                    </p>
                  </div>
                </div>


                <div>
                  <a id="addcart{{ $food->id }}" data-id="{{ $food->id }}" class="btn btn-outline-indigo addcard"> ADD CART</a>
                  <br>
                  <br>
                  <form></form>
                  <a class="btn btn-outline-primary" id="moreInfo" onclick="moreInfo({{ $food->id }})">More Information</a>
                  <br>
                  <br>
                </div>
              </div>
            </div>
            <?php ++$i ?> @endif
            <br>  @endforeach
            @endforeach
          </div>
        </div>
        <div class="card-footer text-muted">
         
      </div>
      <br>
      <br> 

    </div>
 

@endif

<!--Food Items Offer Ends Here ....................-->



<!--Food Items Special Start Here ....................-->

@if(!$specials->isEmpty())

<div  id="addcartOfferDiv">
    
      <div class="card text-center welcomeCard" >
        <div class="card-header">
          <h2>
            <strong style="color:red"> Special Food Items </strong>
          </h2>
        </div>     <?php 
            $count = $specials->count();
            if($count == 1){
              $col_sm = 12 ;
              $col_md = 12 ;
              $col_lg = 12 ;
            }elseif ($count == 2) {
              $col_sm = 12 ;
              $col_md = 5 ;
              $col_lg = 5 ;
            }elseif($count == 3){
              $col_sm = 12 ;
              $col_md = 4 ;
              $col_lg = 4 ;
            }else{
              $col_sm = 3 ;
              $col_md = 12 ;
              $col_lg = 3 ;
            }?>
 
        <br> 
        <div class="card-block" >
     
          <div class="row"> @foreach($specials as $special)
            <?php $i = 0;?> @foreach($foods as $food) @if($food->id == $special->id) @if($i>=4) @break; @endif
            <div class="col-md-{{$col_md}} col-sm-{{$col_sm}} col-lg-{{$col_lg}}">
              <div class="card" id="cardInfo{{ $food->id }}">
                <div class="card-block">

                  <img class="card-img-top" src="/storage/upload/foodImage/{{$food->foodImg}}" class="img-responsive" id="Imagecardfor" alt="Card image cap">
                  <br>
                  <h4 class="card-title">{{ $food->foodname }}</h4>
                  <div class = "back" id="back{{ $food->id }}">
                    <p class="card-text ">{{ $food->description }}</p>
                    <br>
                    <br>
                  </div>
                  <div id="front{{ $food->id }}">
                    <span class="badge badge-success">
                      <p id="cardSize{{ $food->id }}">{{ $food->size_name }}</p>
                    </span>
                    <p>RS.
                      <span id="cardPrice{{ $food->id }}" data-id="{{ $food->price }}">{{ $food->price }}
                    </p>
                  </div>
                </div>


                <div>
                  <a id="addcart{{ $food->id }}" data-id="{{ $food->id }}" class="btn btn-outline-indigo addcard show"> ADD CART</a>
                  <br>
                  <br>
                  <form></form>
                  <a class="btn btn-outline-primary" id="moreInfo" onclick="moreInfo({{ $food->id }})">More Information</a>
                  <br>
                  <br>
                </div>
              </div>
            </div>
            <?php ++$i ?> @endif
            <br>  @endforeach
        @endforeach  </div>
        </div>
        <div class="card-footer text-muted">

        </div>
      </div>
      <br>
      <br> 

    </div>
 

@endif


<!--Food Items Special Ends Here ....................-->


<!--Food Items Start Here ....................-->


     <div  id="addcartDiv">  
     <?php     $count = $foodtypes->count();
            if($count == 1){
              $col_sm = 12 ;
              $col_md = 12 ;
              $col_lg = 12 ;
            }elseif ($count == 2) {
              $col_sm = 12 ;
              $col_md = 5 ;
              $col_lg = 5 ;
            }elseif($count == 3){
              $col_sm = 12 ;
              $col_md = 4 ;
              $col_lg = 4 ;
            }else{
              $col_sm = 3 ;
              $col_md = 12 ;
              $col_lg = 3 ;
            }?>
      @foreach($foodtypes as $foodtype)
      <div class="card text-center welcomeCard" id='{{$foodtype->typename}}'>
        <div class="card-header">
          <h2>
            <strong> {{$foodtype->typename}} </strong>
          </h2>
        </div>
        <br>
        <div class="card-block" >

         
          <div class="row">
            <?php $i = 0;?> @foreach($foods as $food) @if($food->typename == $foodtype->typename) @if($i>=4) @break; @endif
            
            <div class="col-md-{{$col_md}} col-sm-{{$col_sm}} col-lg-{{$col_lg}}">
              <div class="card" id="cardInfo{{ $food->id }}">
                <div class="card-block">

                  <img class="card-img-top" src="/storage/upload/foodImage/{{$food->foodImg}}" class="img-responsive" id="Imagecardfor" alt="Card image cap">
                  <br>
                  <h4 class="card-title">{{ $food->foodname }}</h4>
                  <div class = "back" id="back{{ $food->id }}">
                    <p class="card-text ">{{ $food->description }}</p>
                    <br>
                    <br>
                  </div>
                  <div id="front{{ $food->id }}">
                    <span class="badge badge-success">
                      <p id="cardSize{{ $food->id }}">{{ $food->size_name }}</p>
                    </span>
                    <p>RS.
                      <span id="cardPrice{{ $food->id }}" data-id="{{ $food->price }}">{{ $food->price }}
                    </p>
                  </div>
                </div>


                <div>
                  <a id="addcart{{ $food->id }}" data-id="{{ $food->id }}" class="btn btn-outline-indigo addcard"> ADD CART</a>
                  <br>
                  <br>
                  <form></form>
                  <a class="btn btn-outline-primary" id="moreInfo" onclick="moreInfo({{ $food->id }})">More Information</a>
                  <br>
                  <br>
                </div>
              </div>
            </div>
            <?php ++$i ?> @endif
            <br> @endforeach
             
          </div>
        </div>
        <div class="card-footer text-muted">
          
        </div>
      </div>
      <br>
      <br> @endforeach

    </div>
 



      <!--Food Items Ends Here ....................-->

      <!-- Aboutus Starts Here................ -->

     <div class="card text-center welcomeCard" id="aboutus">
  <div class="card-header">
    <h2>
      <strong> ABOUT US </strong>
    </h2>
  </div>
  <br>
  <div class="card-block">
    <img src="/storage/upload/shoplogo/{{ $shops->shoplogo }}" class="img-responsive waves-image" id="adoutusIma"></img>
    <div class="card-block">
      <p class="card-text">
        <strong>
          <h4>
            <p id="adoutp">Shop Contact Number :-
              <span align="center" id="aboutlabel">{{$shops->phoneno}}</span>
            </p>
            <br>
            <p id="adoutp">Shop Address :-
              <span align="center" id="aboutlabel">{{$shops->address}}</span>
            </p>
          </h4>
        </strong>
      </p>
    </div>
  </div>
</div>
<!-- Aboutus Ends Here................ -->
<br>
<br>

<!-- Comment Starts Here................ -->
<div class="card text-center " id="comment">
  <div class="card-header">
    <h2>
      <strong> COMMENT </strong>
    </h2>
  </div>
  <br>
  <div class="card-block">
    <div class="card-block">
      <p class="card-text">
        <div class="form-group">

          <div class="fb-comments" data-href="{{ Request::url() }}" data-width = "1000" data-numposts="5"></div>
          <br>
          <br>

         <hr>

         <br>
         <br>
         <h3><center><strong>Leave a Message Without FaceBook</strong></center></h3>
            <div class="column is-12">
              <label class="label" for="email">Email</label>
             
                <input class="form-control" id = "commentEmail" name="email" type="text" placeholder="Email">
              
              
            </div>
            <br>
            <div class="form-group">
              <textarea type="text" name="comment" id = "commentText" class="form-control" placeholder="Comments here........"></textarea>
            </div>
          <input type = "hidden" name = "shop_id" id = "commentShop_id" value="{{ $shops->id }}" >
            <a  class="btn btn-green btn-lg commentSubmit">submit</a>

          
          <a href="/comment" class="btn btn-info">View Comments</a>
        </div>
      </p>
    </div>
  </div><br>
<br>
<br>
</div>

<!-- Comment Ends Here................ -->


      @endsection