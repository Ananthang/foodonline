@extends('layouts.user')
@section('title','Foods')

@section('content')

<!--Add Card Model Start Here.........-->
@if(!empty($orderTs))
<div id="addcartlist" class="modal fade " role="dialog">
  <div class="modal-dialog modal-lg">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <h4 class="modal-title">Add Carts</h4>
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <br/>

      </div>
      <div class="modal-body ">



        <table class="table table-striped table-bordered table-hover addCartList">
          <thead class="">
            <tr class="">
              <th>Food Name</th>
              <th>Image</th>
              <th>Size</th>
              <th class="quantityTitle">Qty</th>
              <th>Price (Rs.)</th>
              <th>Update</th>
              <th>Delete</th>
            </tr>
          </thead>
          <tbody>
            @foreach($orderTs as $orderT)

            <tr>
              <td>{{ $orderT->foodname }} {{$orderT->id}}</td>
              <td>
                <img src="/storage/upload/foodImage/{{$orderT->foodImg}}" id="showImage" class="img-fluid" alt="{{$orderT->foodname}}"></img>
              </td>

              <td>{{ $orderT->size_name }}</td>
              <td>
                <input type="text" value="{{$orderT->quantity}}" placeholder="Qty" id="quantity{{$orderT->id}}" data-id="{{$orderT->id}}"
                  class="form-control quantity">
              </td>
              <td id="price{{$orderT->id}}" data-id="{{ $orderT->price}}" data-last-value="{{ $orderT->id }}" class="orderTPrice">
                <?php 
           $mut  = (int) $orderT->price*(int)$orderT->quantity
             ?>{{$mut}}</td>
              <td>

                <a id="updateAddId{{ $orderT->id }}" data-id="{{ $orderT->id }}" class="updateAddCart">
                  <img id="modalImage" src="/img/user/Text-Edit-icon.png">
                </a id="">

              </td>
              <td>
                <a id="deleteAddId{{ $orderT->id }}" data-id="{{ $orderT->id }}" class="deleteAddCart">
                  <img id="modalImage" src="/img/user/delete-icon.png">
                </a>
              </td>


            </tr>
            @endforeach
          </tbody>

        </table>
        <table class="table table-striped table-bordered table-hover">
          <thead class="">
            <tr class="">
              <th>
                <strong>Total (Rs.)</strong>
              </th>
              <th id="total"></th>

            </tr>
          </thead>

        </table>
      </div>
      <div class="modal-footer">
        <a onclick="checkOut()" class="btn btn-success" data-dismiss="modal">Check out </a>
      </div>
    </div>

  </div>
</div>
@endif

<!--Add Card Model Ends Here.........-->


<div class="container">
        <div  id="addcartDiv">
      @foreach($foodtypes as $foodtype)
      <div class="card text-center welcomeCard" id='{{$foodtype->typename}}'>
        <div class="card-header">
          <h2>
            <strong> {{$foodtype->typename}} </strong>
          </h2>
        </div>
        <br>
        <div class="card-block" >

          <div class="row">
            <?php $i = 0;?> @foreach($foods as $food) @if($food->typename == $foodtype->typename) 
            <div class="col-md-3 col-sm-12 col-lg-3">
              <div class="card" id="cardInfo{{ $food->id }}">
             
                <div class="card-block">

                  <img class="card-img-top" src="/storage/upload/foodImage/{{$food->foodImg}}" class="img-responsive" id="Imagecardfor" alt="Card image cap">
                  <br>
                  <h4 class="card-title">{{ $food->foodname }}</h4>
                  <div class = "back" id="back{{ $food->id }}">
                    <p class="card-text ">{{ $food->description }}</p>
                    <br>
                    <br>
                  </div>
                  <div id="front{{ $food->id }}">
                    <span class="badge badge-success">
                      <p id="cardSize{{ $food->id }}">{{ $food->size_name }}</p>
                    </span>
                    <p>RS.
                      <span id="cardPrice{{ $food->id }}" data-id="{{ $food->price }}">{{ $food->price }}
                    </p>
                  </div>
                </div>


                <div>
                  <a id="addcart{{ $food->id }}" data-id="{{ $food->id }}" class="btn btn-outline-indigo addcard"> ADD CART</a>
                  <br>
                  <br>
                  <form></form>
                  <a class="btn btn-outline-primary" id="moreInfo" onclick="moreInfo({{ $food->id }})">More Information</a>
                  <br>
                  <br>
                  <br>
                </div>
              </div>
            </div>
            <?php ++$i ?> @endif
            <br> @endforeach
          </div>
        </div>
        <div class="card-footer text-muted">
          
        </div>
      </div>
      <br>
      <br> @endforeach

    </div>
</div>
{{ $foods->links() }}
@endsection