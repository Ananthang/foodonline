<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
     <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>
    <!-- Font ../Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/font.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/datatables.min.css" rel="stylesheet">
    <link href="/css/jquery-ui.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="/css/mert.min.css" rel="stylesheet">
    <link href="/css/waves.min.css" rel="stylesheet">
    <link href="/css/slider.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
  

 
<!-- Include Date Range Picker -->

<link rel="stylesheet" type="text/css" href="/css/daterangepicker.css" />


    <!-- Your custom styles (optional) -->
    <link href="/css/styleuser.css" rel="stylesheet">
    <link href="/css/customboostrap.css" rel="stylesheet">
    <!--icon Design -->
    <link rel="stylesheet" href="/css/icons/icomoon/styles.css">
    
    
    <link rel="calander" href="https://calendar.google.com/calendar?cid=b25wNjR0cGNyODJnZWExYnVsZjM2bzkyNmNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ">



</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">

    <!-- Start your project here-->
    <!-- SideNav slide-out button -->
    <header>

        <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary scrolling-navbar">
            <a class="navbar-brand" href="/home">
                <strong></strong>
            </a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent"
                aria-expanded="false" aria-label="Toggle navigation">
                <span class="navbar-toggler-icon"></span>
            </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li>
                        <img src="/storage/upload/shoplogo/{{ $shops->shoplogo }}" style="wigth:20px;  height:30px;" class="img-reposive">
                    </li>
       
   
                   <li class="nav-item dropdown">
                        <a class="nav-item nav-link dropdown-toggle" id="navId"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">Food Types</a>
                        <div class="dropdown-menu">
                            @foreach ($foodtypes as $foodtype)
                            <a class="dropdown-item"  href="#{{$foodtype->typename}}">{{$foodtype->typename}}</a>
                            
                               @endforeach
                            
                            
                        </div>
                    </li>

                    <li class="nav-item">
                        <a class="nav-link" id="navId" href="#aboutus">ABOUT US</a>
                    </li>
                    <li class="nav-item">
                        <a class="nav-link" id="navId"  hrefnu ="#comment" >Comment</a>
                    </li>
                </ul>
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <form action="/order/temp" method="POST" enctype="multipart/form-data">
                             {{ csrf_field() }}
                            <input id="arrayOfOrder" type="hidden" name="array" value="">
                            <input id="countOf" type="hidden" name="count" value="">
                            
                            <a  id="butNav"  data-toggle="modal" data-target="#addcartlist">
                            <i class="fa fa-shopping-cart float-icon fa-2x" aria-hidden="true"></i> Addcarts
                        <span class="badge badge-danger" id="count" ></span>
                        </a>
                    </form>
                    </li>

                    <li>
                        <span>
                        <input type="text" data-id="search" class="form-control" id="searchBoxId" >
                    </span>
                    </li>


                </ul>
            </div>
        </nav>


    </header>
    <!--/. Sidebar navigation -->
    <!-- /Start your project here-->
    <main>


        <div class="page-container">



            <div class="wrapper">

              <br>
            <br>
            <br>
            <br/>



<div  id="addcartDiv">  
     <?php     $count = $foodtypes->count();
            if($count == 1){
              $col_sm = 12 ;
              $col_md = 12 ;
              $col_lg = 12 ;
            }elseif ($count == 2) {
              $col_sm = 12 ;
              $col_md = 5 ;
              $col_lg = 5 ;
            }elseif($count == 3){
              $col_sm = 12 ;
              $col_md = 4 ;
              $col_lg = 4 ;
            }else{
              $col_sm = 3 ;
              $col_md = 12 ;
              $col_lg = 3 ;
            }?>
      <div class="card text-center welcomeCard" id='{{$foodtype->typename}}'>
        <div class="card-header">
          <h2>
            <strong> Search List Food Details </strong>
          </h2>
          <a href="/" class = "btn btn-success" value = "HomePage">
        </div>
        <br>
        <div class="card-block" >

         
          <div class="row">
            <?php $i = 0;?> @foreach($foods as $food)  @if($i>=4) @break; @endif
            
            <div class="col-md-{{$col_md}} col-sm-{{$col_sm}} col-lg-{{$col_lg}}">
              <div class="card" id="cardInfo{{ $food->id }}">
                <div class="card-block">

                  <img class="card-img-top" src="/storage/upload/foodImage/{{$food->foodImg}}" class="img-responsive" id="Imagecardfor" alt="Card image cap">
                  <br>
                  <h4 class="card-title">{{ $food->foodname }}</h4>
                  <div class = "back" id="back{{ $food->id }}">
                    <p class="card-text ">{{ $food->description }}</p>
                    <br>
                    <br>
                  </div>
                  <div id="front{{ $food->id }}">
                    <span class="badge badge-success">
                      <p id="cardSize{{ $food->id }}">{{ $food->size_name }}</p>
                    </span>
                    <p>RS.
                      <span id="cardPrice{{ $food->id }}" data-id="{{ $food->price }}">{{ $food->price }}
                    </p>
                  </div>
                </div>


                <div>
                  <a id="addcart{{ $food->id }}" data-id="{{ $food->id }}" class="btn btn-outline-indigo addcard"> ADD CART</a>
                  <br>
                  <br>
                  <form></form>
                  <a class="btn btn-outline-primary" id="moreInfo" onclick="moreInfo({{ $food->id }})">More Information</a>
                  <br>
                  <br>
                </div>
              </div>
            </div>
            <?php ++$i ?> 
            <br> @endforeach
             
          </div>
        </div>
        <div class="card-footer text-muted">
          <button class="btn btn-info" href="#"> SEE MORE </button>
        </div>
      </div>
      <br>
      <br>

    </div>



    </main>
    <!--/main-->
  <footer>
    <div class="footer" id="footer">
        <div class="container">
            <div class="row"><h3> <strong>Shop Details</strong> </h3>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                    
                    <ul>
                        <li> <a href="#"> <strong>Shop Name</strong> </a> </li>
                        <li> <a href="#"> <strong>Shop Address</strong> </a> </li>
                        <li> <a href="#"> <strong>Shop Phone Number</strong> </a> </li>
                        <li> <a href="#"> <strong>Shop Gmail Address</strong> </a> </li>
                    </ul>
                </div>
                <div class="col-lg-4  col-md-4 col-sm-4 col-xs-6">
                    
                    <ul>
                        <li> <a href="#"> <strong>{{ $shops->shopname }}</strong> </a> </li>
                        <li> <a href="#"> <strong>{{ $shops->address }}</strong> </a> </li>
                        <li> <a href="#"><strong> {{ $shops->phoneno }}</strong> </a> </li>
                        <li> <a href="#"> <strong>{{ $shops->email }} </strong></a> </li>
                    </ul>
                </div>
               
                <div class="col-lg-3  col-md-3 col-sm-6 col-xs-12 ">
                    
                    <ul class="social">
                        <li> <a href="www."> <i class=" fa fa-facebook">   </i> </a> </li>
                        
                    </ul>
                </div>
            </div>
            <!--/.row--> 
        </div>
        <!--/.container--> 
    </div>
    <!--/.footer-->
    
    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left"> Copyright © Footer 2014. All right reserved. </p>
            
        </div>
    </div>
    <!--/.footer-bottom--> 
</footer>
    <!--/Footer-->

    <!--/Footer-->


    <!-- SCRIPTS -->
    <!-- JQuery -->

    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <script type="text/javascript" src="/js/jquery-ui.js"></script>
    <script type="text/javascript" src="/js/dist/jquery.validate.js"></script>

    <!-- Bootstrap tooltips
    <script type="text/javascript" src="/js/popper.min.js"></script>
    Bootstrap core JavaScript -->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/waves.min.js"></script>
    <script type="text/javascript" src "https://cdnjs.cloudflare.com/ajax/libs/Chart.js/2.7.1/Chart.bundle.js"></script>
    <script type="text/javascript" src "/js/justfilp.min.js"></script>
    <script src="https://cdn.jsdelivr.net/npm/vue"></script>
  <script type="text/javascript" src="/js/sweetalert2.js"></script>
  <script type="text/javascript" src="/js/datatables.min.js"></script>
  <script type="text/javascript" src="/js/ScrollMagic.min.js"></script>
  <script type="text/javascript" src="/js/slider.js"></script>

 <script type="text/javascript" src="/js/moment.js"></script>
<script type="text/javascript" src="/js/daterangepicker.js"></script>

  <script type="text/javascript" src="/js/addIndicators.min.js"></script>
    <script type="text/javascript" src="/js/customuser.js"></script>
   
<script>
 
    </script>
   
    
</body>

</html>

 
