<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Offers extends Model
{
    protected $fillable = [
        'food_sizes_id', 'shop_id','orignal_price'
    ];

    public function shop(){
        return $this->belongTo('App\Shop' , 'shop_id');
    }
}
