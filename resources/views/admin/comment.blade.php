@extends('layouts.homenav')

@section('title','User Comment')

@section('content')
<br>
<br>
<br>
<div class="card text-center">
  <div class="card-header">
    <h2>
      <strong> Comments</strong>
    </h2>
  </div>
  <br>
  <div class="card-block">
    
    <div class="card-block" id="commentTable">
        
         <table class="table table-striped table-bordered table-hover display commentTable" id = "TableId" >
           <thead>
              
              <th>Email Address</th>
              <th>Comment</th>
              <th>Time</th>
            </thead><tbody>
            @foreach($comments as $comment)
            <tr>
             
                
                 <td id = "commentImaEmail">{{ $comment->emailaddress }}</td>
                 <td>{{ $comment->comment }} </td>
                 <td id = "commentTime">{{ $comment->created_at->diffForHumans() }} </td>
             </tr>
            @endforeach
          </tbody>
         </table>
     
    </div>
  </div>
</div>

<br>

              
          


@endsection