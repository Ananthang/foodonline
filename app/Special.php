<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Special extends Model
{
     protected $fillable = [
       'food_sizes_id', 'shop_id',
    ];
    public function shop(){
        return $this->belongTo('App/Shop','shop_id');
    }

}
