<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class ShopSale extends Model
{
     protected $fillable = [
       'shop_id' , 'order_id' , 'total',
    ];
    
    public function order_id(){
      return $this->belongTo('App\Orders','order_id');
    }

    public function shop(){
      return $this->belongTo('App\Shop','shop_id');
    }

}
