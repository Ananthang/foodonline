<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order_items extends Model
{
    protected $fillable = [
    'order_id'  , 'food_sizes_id' , 'quantity' ,
];

    public function food_sizes()
    {
        return $this->hasOne('App\FoodSize','id');
    }
    public function order()
    {
        return $this->hasOne('App\Order','order_id');
    }
}
