/* Class BinTreeNode */
 class BinTreeNode
 {
     BinTreeNode leftChild, rightChild;
     int val;
 
     public BinTreeNode()
     {
         leftChild  = null;
         rightChild = null;
         val        = 0   ;
     }
     
     public BinTreeNode(int n)
     {
         leftChild = null;
         rightChild = null;
         val = n;
     }
     
     public void setLeftChild(BinTreeNode n)
     {
         leftChild = n;
     }
     
     public void setRightChild(BinTreeNode n)
     {
         rightChild = n;
     }
     
     public BinTreeNode getLeftChild()
     {
         return leftChild;
     }
     
     public BinTreeNode getRightChild()
     {
         return rightChild;
     }
     
     public void setVal(int d)
     {
         val = d;
     }
     
     public int getVal()
     {
         return val;
     }     
 }
 
