<?php

namespace App\Http\Controllers;

use App\OrderTem;
use Illuminate\Http\Request;
use App\Order;
use App\Customer;
use App\User;
use App\FoodType;
use App\Shop;
use App\FoodItem;
use App\FoodSize;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Response;

class OrderTemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
     
     //  $orderTs = OrderTem::latest()->paginate(5);
    $shop_id = session('shop_id');
  $code_id = session('code_id');
       $data = array(
         //  'orderT' => OrderTem::all(),
         'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
 'shops' =>Shop::find(session()->get('shop_id')),
           'orderTs' => DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('order_tems','order_tems.food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','order_tems.quantity','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where([['food_types.shop_id','=',session()->get('shop_id')],['order_tems.code_id','=',session()->get('code_id')]])
            ->get(),
       );

        return view('users.order')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $orderT = new OrderTem;
        $size_id = $request->food_sizes_id;
        $code_id = $request->code_id;
        $quantity = $request->quantity;

        $orderT->food_sizes_id = $size_id;
        $orderT->code_id = $code_id;
        $orderT->quantity = $quantity;


        if($orderT->save()){
                     $data = array(
    'foodtypes'  =>  FoodType::all(),
    'foods'   =>  DB::table('food_items')
                ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
                ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
                ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id')
                ->get(),
    'shops' =>Shop::find(session()->get('shop_id')),
    'orderTs' => DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('order_tems','order_tems.food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','order_tems.quantity','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where([['food_types.shop_id','=',session()->get('shop_id')],['order_tems.code_id','=',session()->get('code_id')]])
            ->get(),
    
);
  session()->put('code_id', $code_id);
     

        return $data ;
 
        }
        else{

        }
               
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\OrderTem  $orderTem
     * @return \Illuminate\Http\Response
     */
    public function show(OrderTem $orderTem)
    {
         
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\OrderTem  $orderTem
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $orderT = OrderTem::find($id);
        $size =   FoodSize::find($orderT->food_sizes_id);
        $food = FoodItem::find($size->food_items_id);
        $type = FoodType::find($food->id);
         $data = array(
            'shops' =>Shop::find(session()->get('shop_id')),
                'sizes' => FoodSize::find($id),
                'foods' => FoodItem::find($size->food_items_id),
            'types' => FoodType::find($food->id),
            'foodtypes' =>FoodType::all(),
        );
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\OrderTem  $orderTem
     * @return \Illuminate\Http\Response
     */
    public function updateItem(Request $request)
    {
      
        $id = $request->food_sizes_id;
       // $orderT = DB::table('order_tems')
//->where('order_temps.food_sizes_id','=',$id)->get();  
        //$sizes =   FoodSize::find($orderT->food_sizes_id);

        $orderT = OrderTem::where([['food_sizes_id' , $id],['code_id','=',$request->code_id]] )->first();

        //$orderT = orderTem::find($orderT->id);
    

        $orderT->food_sizes_id = $id ;
        $orderT->code_id = $request->code_id ;
        $orderT->quantity =  $request->quantity ;


        if($orderT->save()){ 
          
            $data = array(
    'foodtypes'  =>  FoodType::all(),
    'foods'   =>  DB::table('food_items')
                ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
                ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
                ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id')
                ->get(),
    'shops' =>Shop::find(session()->get('shop_id')),
    'orderTs' => DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('order_tems','order_tems.food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','order_tems.quantity','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where([['food_types.shop_id','=',session()->get('shop_id')],['order_tems.code_id','=',session()->get('code_id')]])
            ->get(),
    
);
             return $data;
        }
        else{
                return 'hi';
        }


    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\OrderTem  $orderTem
     * @return \Illuminate\Http\Response
     */
    public function destroyItem($id)
    {
        $orderT = OrderTem::where([['food_sizes_id',$id],['code_id','=',session()->get('code_id')]])->first();
        $orderT->delete();
        
       $data = array(
    'foodtypes'  =>  FoodType::all(),
    'foods'   =>  DB::table('food_items')
                ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
                ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
                ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id')
                ->get(),
    'shops' =>Shop::find(session()->get('shop_id')),
    'orderTs' => DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('order_tems','order_tems.food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','order_tems.quantity','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where([['food_types.shop_id','=',session()->get('shop_id')],['order_tems.code_id','=',session()->get('code_id')]])
            ->get(),
    
);
             return $data;
    }
   
}
