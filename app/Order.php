<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Order extends Model
{
     protected $fillable = [
    'customer_id' , 'status' ,'order_date_time','order_type','delivery_type',
];

public function customer()
    {
        return $this->hasOne('App\Customer','id');
    }
public function order_items()
{
    return $this->hasMany('App\Order_items','order_id');
}
public function shopsale(){
    return $this->hasOne('App\ShopSales','order_id');
}
}
