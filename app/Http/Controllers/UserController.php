<?php

namespace App\Http\Controllers;

use App\User;
use Illuminate\Http\Request;
use Auth;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;

class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('anan')){
         session()->forget('anan');
       return view('admin.auth.register');}
       else{
            return  redirect('/register');
       }

    }

/*

    public function showlogin()
    {

        return view('admin.login');

    }


    public function login(Request $request)
    {

        $this()->validate($request , [
                'username' =>'required|unique:posts|max=255',  
                'password' =>'required|max=255', 
            ]);
        if(Auth::attempt(['username'=>$request->username,'password'=>$request->password])){
            return view('admin.home');
        }
        else{
            return  "sjdghchsd";
        }
        
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        
           $validator = Validator::make($request->all(), [
            'shop_id'  =>'required',
            'username' =>'required|string|max:255|unique:users,username',
            'name' => 'required|string|max:255|',
            'userImg'  =>'required',
            'password' =>'required|confirmed|max:255', 
        ]);


        if($validator->fails()){
             return redirect('/reg')
                        ->withErrors($validator)
                        ->withInput();
        }
        else{
            if($request->hasFile('userImg')){
                $userImg = $request->file('userImg');
                $username = $request->username;
                $name = $request->name;
                $shop_id = $request->shop_id;
                $password = $request->password ;
            
            
                
                $filename = $username.'.'.$userImg->getClientOriginalExtension();
                $request->userImg->storeAS('public/upload/userImage',$filename);

              

                $user = new User;
                $user->userImg = $filename;
                $user->shop_id = (int)$shop_id;
                $user->username = $username;
                $user->name = $name;
                $user->password  = bcrypt($password) ;
        





                if($user->save()){
                    
                    Session::flash('user', 'your user is successfully created . please insert your owner details here.If your like edit your profile go to PROFILE' );
                            
                    return  redirect('/admin');
                }

                Session::flash('shop', 'your Shop is not created . please try again laters');
                return  redirect('/reg');
            
            }

            else{

                $userImg = 'defaul1t.jpg';
                $username = $request->username;
                $name = $request->name;
                $shop_id = $request->shop_id;
                $password = $request->password ;
            
            
                
               
                $user = new User;
                $user->userImg = 'storage\upload\logo\default1.jpg';
                $user->shop_id = $shop_id;
                $user->username = $username;
                $user->name = $name;
                $user->password  = bcrypt($password) ;
        





                if($user->save()){
                    
                Session::flash('user', 'your user is created . please insert your owner details here.
                If your like edit your profile go to PROFILE ' );
               return  redirect('/admin');
                }

                Session::flash('shop', 'your Shop is not created . please try again laters');
                return  redirect('/reg');
            }
        
        }

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
       
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
}
