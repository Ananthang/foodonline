<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
//user create register link
Route::get('/register','ShopUserController@shopreg');
Route::POST('/register','ShopUserController@check');
Route::resource('/anan', 'ShopController');
Route::resource('/reg','UserController');


// shop admin link
Route::resource('/admin','HomeController');
Route::get('/admin/home/user','ShopUserController@index');
Route::POST('/admin/home/logout','ShopUserController@logout');
Route::get('/admin/home/user/shopdet','ShopUserController@details');
Route::get('/admin/home/order','ShopUserController@showOrder');
Route::get('/admin/home/comment','ShopUserController@showcomment');
Route::get('/admin/home/order/{id}/editOrder','ShopUserController@editOrder');
Route::POST('/admin/home/order/update','ShopUserController@updateOrder');
Route::PUT('/admin/home/user/shopdet','ShopUserController@update');
Route::resource('/admin/home/sales','ShopSaleController');
Route::resource('/admin/home/type','FoodTypeController');

Route::resource('/admin/home/quantity','FoodSizeController');
Route::resource('/admin/home/fooditem','FoodItemController');

Route::resource('/admin/home/user/profile' ,'UserProfileController');
Route::resource('/admin/home/offer','OffersController');
Route::GET('/admin/home/offer/delete/{id}','OffersController@destroyItem');
Route::resource('/admin/home/special','SpecialController');
Route::GET('/admin/home/special/delete/{id}','SpecialController@destroyItem');



//shop customer link
Route::GET('fooditems/search/{search}','SearchController@index');
Route::GET('fooditems/search/more/{searchId}','SearchController@searchId');
Route::resource('/','CustomerController');
Route::resource('/fooditems','userFoodController');
Route::resource('/order','orderController');
Route::resource('/order/addcart/temp','OrderTemController');
Route::POST('/order/addcart/temp/update','OrderTemController@updateItem');
Route::GET('/order/addcart/temp/delete/{id}','OrderTemController@destroyItem');
Route::resource('/comment','CommentController');