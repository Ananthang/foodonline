<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shop User Login Form </title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/font.min.css">
    <!-- Bootstrap core CSS -->
    <link href="css/bootstrap.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="css/stylelogin.css" rel="stylesheet">
</head>

<body>

    <!-- Start your project <here-->
    <main>
        <div class="container">
            <br>
            <br>
            <br>
            <br>

            <div class="row">
                <div class="col-md-3"></div>
                <div class="col-md-6">
                    <center>
                        <h1 id="h2">Login</h1>
                        <br>
                    </center>
                    <div id="form">
                        <div class="card card-cascade" id="loginImg">
                            <div class="view overlay hm-white-slight">
                                <center>

                                </center>
                                <div class="mask"></div>
                                </a>
                            </div>
                            <div class="card-body" >
                                <form action="login.php" method="post">
                                    <div class="form-group">
                                        <h4> <label id="h2">Username</label></h4><br>
                                        <input type="text" class="form-control" name="username" placeholder="Username" required><br>
                                        <h4><label id="h2">Password</label></h4><br>
                                        <input type="password" class="form-control" name="password" placeholder="Password" required><br>
                                        <a class="btn btn-primary" value="submit">submit</a>
                                    </div>

                                </form>
                            </div>
                        </div>
                    </div>
                </div>


            </div>
            <div class="col-md-3"></div>
        </div>


    </main>
    </here-->
    <!-- /Start your project here-->

    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
</body>

</html>