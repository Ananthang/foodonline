@extends('layouts.homenav')

@section('title','Shop Admin')

@section('content')
<br>
<br>


<br>
<div class="container">


<div class="panel-body">
        @if (session('login'))
            <div class="alert alert-success alert-dismissible">
                <a href="#" class="close" data-dismiss="alert" aria-label="close">&times;</a>
                {{session()->get('login')}}
            </div>
        @endif
    </div>
<div class="row">
   
    <div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card" id="cardAddcart">
            <div class="card-body">
                <img src="/img/user/add cart.png" id="cardImg" alt="addcards" ><span class="badge badge-danger">{{$orders->count()}}</span> 
                <br>New Orders
                <a class="btn btn-primary" href = "/admin/home/order">view</a>
            </div>
        </div>
    </div>


    


    <div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card" id="cardComment">
           
            <div class="card-body">
                 <img src="/img/user/comment(1).png" id="cardImg" alt="comment"><span class="badge badge-danger">{{$comments->count()}}</span>
             <br>  New Comments
             
                <a class="btn btn-primary" href = "/admin/home/comment">view</a>
            </div>
        </div>
    </div>


    <div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card card-cascade narrower" id="cardHistory">
            
            <div class="card-body"><img class="img-circle" src="/img/user/history.png" id="cardImg" alt="saleshistory">
                <br> Sales
               
            
                <a class="btn btn-primary" href = "/admin/home/sales">view</a>
            </div>
        </div>
    </div>
    

   


<br>
<br>
<br>


</div>
<br>
<br>
<div class="row">
<div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card card-cascade narrower" id="cardHistory">
<br>
                <a class="btn btn-primary btn-lg"  href="/admin/home/fooditem">Food Items</a><br>
            </div>
</div>
   
<div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card card-cascade narrower" id="cardHistory">
<br>
                <a class="btn btn-primary btn-lg" href="/admin/home/type">Food Type</a><br>
            </div>
        </div>

<div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card card-cascade narrower" id="cardHistory"><br>
               <a class="btn btn-primary btn-lg" href="/admin/home/offer">Add Offers</a><br>
            </div>
        </div>
<br><br><br>
<br><br>
<br>
<div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card card-cascade narrower" id="cardHistory"><br>
                <a class="btn btn-primary btn-lg" href="/admin/home/special">Add Special FoodItem</a><br>
            </div>
        </div>
<div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card card-cascade narrower" id="cardHistory"><br>
                 <a href="/admin/home/user/profile"  class="btn btn-primary btn-lg"><i class="icon-user-plus"></i><span>Profile</span></a>
            <br></div>
        </div>
<div class="col-md-4 col-sm-10 col-lg-4">
        <div class="card card-cascade narrower" id="cardHistory"><br>
               <a class="btn btn-primary btn-lg" id="navId" href="/anan/{{session()->get('shop_id')}}/edit">Shop Details</a><br>
            </div>
        </div>

     


</div>
<!--/main body page-->
@endsection