<?php

namespace App\Http\Controllers;

use App\FoodType;
use Illuminate\Http\Request;
use App\Shop;
use Illuminate\Support\Facades\Validator;
use Session;
use Illuminate\Support\Facades\DB;
    use Illuminate\Support\Facades\Redirect;

class FoodTypeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
    if(session()->has('shop_id')){

        $shop_id = session('shop_id');
      $data = array(
                   'foodtypes'=>DB::table('food_types')
                         ->where('shop_id' ,'=',$shop_id)
                         ->get(),
            'shops' => Shop::find(session()->get('shop_id')),
            );
            
       return view ('admin.type')->with($data);
    }
       else{
           return Redirect::to('/admin');
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(session()->has('shop_id')){

        $validator = Validator::make($request->all(),[
            'typename' =>'required|string|nullable|max:150',
            'shop_id'  =>'required',
        ]);


        if($validator->fails()){
            return redirect('/admin/home/type')
                    ->withErrors($validator)
                    ->withInput();
        }
        else{
            $shop_id = $request->shop_id;
            $typename = $request->typename;

            $type = new FoodType ;

            $type->typename = $typename;
            $type->shop_id  =(int) $shop_id ;
            
            if($type->save()){
                Session::flash('type' ,$typename.' is successfully created ');

                return redirect('/admin/home/type');
            }
            else{
                Session::flash('type' ,$typename.' is not successfully created Try again later');
            }
        }}
       else{
           return Redirect::to('/admin');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodType  $foodType
     * @return \Illuminate\Http\Response
     */
    public function show(FoodType $foodType)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodType  $foodType
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(session()->has('shop_id')){

                $shop_id = session('shop_id');
      $data = array(
                    'foodtypes' => FoodType::find($id),
            'shops' => Shop::find(session()->get('shop_id')),
            );
            
       return view ('admin.type_edit')->with($data);
       }
       else{
           return Redirect::to('/admin');
       }

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodType  $foodType
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        if(session()->has('shop_id')){

        $type = FoodType::find($id);
           $shop_id = $request->shop_id;
            $typename = $request->typename;

           

            $type->typename =ucfirst($typename);
            $type->shop_id  =(int) $shop_id ;
            
            if($type->save()){
                Session::flash('type' ,$typename.' is successfully created ');

                return redirect('/admin/home/type');
            }
            else{
                Session::flash('type' ,$typename.' is not successfully created Try again later');
            }
        }
       else{
           return Redirect::to('/admin');
       }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodType  $foodType
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodType $foodType)
    {
        //
    }
}
