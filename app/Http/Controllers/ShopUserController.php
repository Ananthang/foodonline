<?php

namespace App\Http\Controllers;


use App\Order;
use App\OrderTem;
use App\Order_items;
use App\Customer;
use App\Comment;

use App\FoodType;

use Illuminate\Notifications\Notifiable;
use App\Notifications\TaskCustomerOrder;
use App\FoodItem;
use App\FoodSize;
use Illuminate\Http\Request;
use File;
use Illuminate\Http\UploadedFile;
use App\User;
use App\Shop;
use App\ShopSale;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class ShopUserController extends Controller
{
   public function index()
   {
    if(session()->has('shop_id')){
         $data = array(
             'shops' =>Shop::find(session()->get('shop_id')),
    'foodtypes'  =>  FoodType::where('food_types.shop_id','=',session()->get('shop_id'))->get(),
    'foods'   =>  DB::table('food_items')
                ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
                ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
                ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id')
                ->where('food_types.shop_id','=',session()->get('shop_id'))
                ->get(),
    'shops' =>Shop::find(session()->get('shop_id')),
    'orders' => DB::table('orders')
            ->join('customers','customers.id','=','orders.customer_id')
            ->select('orders.id')
            ->where('customers.shop_id' , '=',session()->get('shop_id'))
            ->get(),
    'customers'=>Customer::where('shop_id','=',session()->get('shop_id'))->get(),
    'comments'=>Comment::where('shop_id','=',session()->get('shop_id'))->get(),
    'specials'   =>  DB::table('food_items')
            
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('specials','food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
    'offers'   =>  DB::table('food_items')
            
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('offers','food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id','offers.orignal_price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
    
);
       session()->flash('login' , 'Your successfully logined in '.Auth::user()->username);
        
       return view('admin.home')->with($data);}
       else{
           return Redirect::to('/admin');
       }
   }


   public function details()
   { 
       if(session()->has('shop_id')){
        $shops = Shop::find(Auth::user()->shop_id);
        $users = User::find(Auth::user()->shop_id);
       return view('admin.shopPro',compact('shops'),compact('users'));}
       else{
            return Redirect::to('/admin');
       }
   }

   public function userprofile()
   {
       if(session()->has('shop_id')){
        return view('admin.profile');
    }
       else{
            return Redirect::to('/admin');
       }
   }



   public function update(Request $request)
   {
        
   }

   public function showOrder(){

     if(session()->has('shop_id')){
 

            $shop_id = session()->get('shop_id');
            $data = array(
            'orders'=>DB::table('orders')
            ->join('customers','customers.id','=','orders.customer_id')
            ->join('order_items','orders.id','=','order_items.order_id')
            ->join('food_sizes','order_items.food_sizes_id','=','food_sizes.id')
            ->join('food_items','food_sizes.food_items_id','=','food_items.id')
            ->select('orders.id','orders.status','orders.status','orders.order_date_time','orders.order_type','orders.delivery_type',
            'customers.customername','customers.address','customers.city','customers.email','customers.phoneno','customers.city','customers.shop_id',
            'order_items.quantity',
            'food_items.foodname','food_items.foodImg',
            'food_sizes.size_name','food_sizes.price'
            )
            ->where('customers.shop_id','=',$shop_id)
            ->get(),
        'shops' =>Shop::find(session()->get('shop_id')),
            
            
        );
       return view('admin.customerorder')->with($data);
    }
        
        else{
            return Redirect::to('/admin');
        }
   }



   public function editOrder($id){
 if(session()->has('shop_id')){
 
        $shop_id = session()->get('shop_id');
       $data = array(
    'orders'=>DB::table('orders')
    ->join('customers','customers.id','=','orders.customer_id')
    ->join('order_items','orders.id','=','order_items.order_id')
    ->join('food_sizes','order_items.food_sizes_id','=','food_sizes.id')
    ->join('food_items','food_sizes.food_items_id','=','food_items.id')
    ->select('orders.status','orders.status','orders.order_date_time','orders.order_type','orders.delivery_type',
    'customers.customername','customers.address','customers.city','customers.email','customers.phoneno','customers.city','customers.shop_id',
    'order_items.quantity','order_items.id',
    'food_items.foodname','food_items.foodImg',
    'food_sizes.size_name','food_sizes.price'
    )
    ->where([['customers.shop_id','=',$shop_id],['order_items.id','=',$id]])
    ->get(),
'shops' =>Shop::find(session()->get('shop_id')),
    
    
);
       return view('admin.customerorderupdate')->with($data);
    }
 else{
            return Redirect::to('/admin');
       }

   }


public function updateOrder(Request $request){



        $id = $request->id;
        $statusnew = $request->status;
    
        $orders = Order::find($id);
           
                $customer_id = $orders->customer_id;
                $order_date_time = $orders->order_date_time;
                $order_type = $orders->order_type;
                $delivery_type = $orders->delivery_type;
            

            $orders->status = $statusnew ;
            $orders->customer_id = $customer_id ;
            $orders->order_date_time = $order_date_time ;
            $orders->order_type = $order_type ;
            $orders->delivery_type = $delivery_type ;
           $orders->save();
            if($statusnew == "Delivered"){
                $sale = new ShopSale;
                $shop_id = session()->get('shop_id');
                $total = $request->total;

                $sale->order_id = $id; 
                $sale->shop_id = $shop_id; 

                
                $sale->total = $total; 

                $sale->save();
            }
$shop_id = session()->get('shop_id');
       $data = array(
    'orders'=>DB::table('orders')
    ->join('customers','customers.id','=','orders.customer_id')
    ->join('order_items','orders.id','=','order_items.order_id')
    ->join('food_sizes','order_items.food_sizes_id','=','food_sizes.id')
    ->join('food_items','food_sizes.food_items_id','=','food_items.id')
    ->select('orders.status','orders.status','orders.order_date_time','orders.order_type','orders.delivery_type',
    'customers.customername','customers.address','customers.city','customers.email','customers.phoneno','customers.city','customers.shop_id',
    'order_items.quantity','order_items.id',
    'food_items.foodname','food_items.foodImg',
    'food_sizes.size_name','food_sizes.price'
    )
    ->where([['customers.shop_id','=',$shop_id],['order_items.id','=',$id]])
    ->get(),
'shops' =>Shop::find(session()->get('shop_id')),
    
    
);
           return $data;


      
}


public function showComment(){
     if(session()->has('shop_id')){
 
 
    $data = array('shops' =>Shop::find(session()->get('shop_id')),
'comments'=>Comment::where('shop_id','=',session()->get('shop_id'))->get());
    return view('admin.comment')->with($data);}
    else{
            return Redirect::to('/admin');
       }
}

public function showCustomer(){
     if(session()->has('shop_id')){
 
   $data = array('shops' =>Shop::find(session()->get('shop_id')),
'comments'=>Comment::where('shop_id','=',session()->get('shop_id'))->get());
    return view('admin.customerdetails')->with($data);
}
 else{
            return Redirect::to('/admin');
       }
}


public function logout(){
session()->forget('shop_id');
return Redirect::to('/admin');
}

public function shopreg(){
    return view('admin.auth.loginanan');
}


public function check(Request $request){

  $username =  $request->username;
    $password = $request->password;


     $count =  DB::table('usersAdmin')->where([['username','=',$username],['password','=',$password]])->count();
    // attempt to do the login
    if ($count>0 ){

        // validation successful!
        // redirect them to the secure section or whatever
        // return Redirect::to('secure');
        // for now we'll just echo success (even though echoing in a controller is bad)

       session()->put('anan',$username);
      return Redirect::to('/anan');
            
  

    } else {        

        // validation not successful, send back to form 
           return Redirect::to('/register');

    }

}
}


