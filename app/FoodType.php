<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodType extends Model
{
    protected $fillable = [
    'typename' , 'shop_id' , 
    ];

    public function fooditem()
    {
        return $this->belongsToMany('App\FoodItem' , 'food_types_id');
    }
}
