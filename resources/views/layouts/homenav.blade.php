<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
   <meta http-equiv="x-ua-compatible" content="ie=edge">
     <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>
    <!-- Font ../Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/font.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
        <link href="/css/datatables.min.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="/css/mert.min.css" rel="stylesheet">
<link href="/css/customboostrap.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="/css/style.css" rel="stylesheet">
    <!--icon Design -->
    <link rel="stylesheet" href="/css/icons/icomoon/styles.css">
    <link rel="calander" href="https://calendar.google.com/calendar?cid=b25wNjR0cGNyODJnZWExYnVsZjM2bzkyNmNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ">
</head>

<body>

    <!-- Start your project here-->
    <!-- SideNav slide-out button -->
    <header>

    <nav class="navbar fixed-top navbar-expand-lg navbar-dark bg-primary scrolling-navbar">
            <a class="navbar-brand" href="/home"><strong></strong></a>
            <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>

            <div class="collapse navbar-collapse" id="navbarSupportedContent">
                <ul class="navbar-nav mr-auto">
                    <li class="nav-item active">
                        <a class="nav-link" href="/admin/home/user">{{$shops->shopname}} <span class="sr-only">(current)</span></a>
                    </li>
                    <li class="nav-item dropdown">
                        <a class="nav-item nav-link dropdown-toggle" id="navId"  data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">EDIT</a>
                        <div class="dropdown-menu">
                           
                            <a class="dropdown-item"  href="/admin/home/fooditem">Food Items</a>
                            <a class="dropdown-item" href="/admin/home/type">Type</a>
                            <a class="dropdown-item" href="/admin/home/offer">Offers</a>
                            <a class="dropdown-item" href="/admin/home/special">Special Fooditem</a>
                           
                            
                  
                        </div>
                    </li>
                    <li class="nav-item">
                    <a class="nav-link" id="navId" href="/anan/{{session()->get('shop_id')}}/edit">Shop Details</a>
                    </li>     
                </ul>
                <ul class="navbar-nav nav-flex-icons">
                    <li class="nav-item">
                        <a href="#" id="navId" class="nav-link dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">{{Auth::user()->username}}</a>
                        <div class="dropdown-menu dropdown-menu-right">
                            <ul>
                                
                                <a href="/admin/home/user/profile"  class="dropdown-item"><i class="icon-user-plus"></i><span>Profile</span></a>
                            <a href="/admin" onclick="event.preventDefault();
                                                     document.getElementById('/-form').submit();" class="dropdown-item">
                                    <i class="fa fa-sign-out" aria-hidden="true"></i>
                                    <span>Logout</span>
                                </a>

                                <form id="/-form" action="/admin/home/logout" method="POST" style="display: none;">
                                    {{ csrf_field() }}
                                </form>
                                
                                
                            </ul>
                        </div>
                    </li>

                </ul>
            </div>
        </nav>


    </header>
    <!--/. Sidebar navigation -->
    <!-- /Start your project here-->
    <main>

        <div class="page-container">


      
                <div class="container-fluid">

                    @section('content') @show
                </div>

                <!--/tap panel content -->
                <!--/main body page-->
            </div>
        
    </main>
    <!--/main-->
    

    <div class="footer-bottom">
        <div class="container">
            <p class="pull-left"> Copyright ©  2018. All right reserved. </p>
            
        </div>
    </div>


    <!-- SCRIPTS -->
    <!-- JQuery -->

    <script type="text/javascript" src="/js/jquery.min.js"></script>
    <!--easy jquery jqueryUI.com-->
       <script type="text/javascript" src="/js/jqueryUI.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="/js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="/js/bootstrap.min.js"></script>
    <script type="text/javascript" src="/js/bootstrapc.min.js"></script>
    <script type="text/javascript" src="/js/canvas.min.js"></script>
      <script type="text/javascript" src="/js/datatables.min.js"></script>

    <!-- MDB core JavaScript -->
<script type="text/javascript" src="/js/sweetalert2.js"></script>
    <script type="text/javascript" src="/js/customadmin.js"></script>
    <script>
      
    </script>

</body>

</html>