@extends('layouts.homenav') 

@section('title','User Profile Edit')

@section('content')
<br>
<br>
<br>

@if(session('success'))
    <div>
        <p class="alert alert-success">
            {{session('success')}}
        </p>
    </div>
@endif
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="card card-cascade">



            <div class="view overlay hm-white-slight">
                <img src="/storage/upload/userImage/{{ Auth::user()->userImg }}"  id="profileImg" class="img-thumbnail" alt="sdfd">
                <a class="waves-effect waves-light"></a>
                <div class="mask"></div>
            </div>

            <h3>Profile</h3>
            <div class="card-body">
                <label class="thicker" for=""><strong>Username</strong></label><p  id="tab1">{{ Auth::user()->username }}</p>
                <br>
               
                <a class="btn btn-primary" data-toggle="modal" data-target="#profileEdit">Edit Profile</a>
            </div>

        </div>
        <div id="profileEdit" class="modal fade" role="dialog">
            <div class="modal-dialog">

                <!-- Modal content-->
                <div class="modal-content">
                    <div class="modal-header">
                       <center> <h4 class="modal-title">Edit Profile</h4></center>
                        <button type="button" class="close" data-dismiss="modal">&times;</button>
                        <br/>
                    </div>
                    <div class="modal-body">
                        <div class="form-group">
                            <form action="" method="POST" id="formReg">
                                {{csrf_field() }}

                           
                                <input name ='shop_id' type="hidden" value="{{session()->get('shop_id')}}">

                                <div class="form-group">
                                    <i class="fa fa-user prefix grey-text"></i><label for="orangeForm-name">Owner Name</label>
                                    <input type="text" id="orangeForm-name" name="name" class="form-control">
                                    
                                </div>

                                <div class="form-group">
                                    <i class="fa fa-image prefix grey-text"></i>  <label for="orangeForm-userImg">Profile picture</label>
                                    <input type="file" id="orangeForm-userImg" name="userImg" class="form-control">
                                  
                                </div>



                                <div class=" form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <i class="fa fa-key prefix grey-text"></i>
                                    <label for="orangeForm-password" class="col-md-4 control-label">Password</label>
                                </div>

                                    <input id="orangeForm-password" type="password" class="form-control" name="password" required> 
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="md-form">
                                    <i class="fa fa-key prefix grey-text"></i>
                                    <label for="orangeForm-password-confirm" class="col-md-4 control-label">Confirm Password</label>
                                    <input id="orangeForm-password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                </div>


                                <div class="text-center">
                                    <button class="btn btn-primary" name="submit" type="submit">Create Owner</button>
                                </div>

                            </form>
                        </div>
                    </div>
                    <div class="modal-footer"></div>
                </div>
            </div>
        </div>
    </div>

    <div class="col-md-3"></div>

</div>
<!--/main body page-->
@endsection