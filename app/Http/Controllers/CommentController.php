<?php

namespace App\Http\Controllers;

use App\Comment;
use Illuminate\Http\Request;
use App\Auth;
use App\Shop;
use App\Customer;
use App\User;
use App\FoodType;
use App\FoodItem;
use App\FoodSize;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Response;



class CommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
              $data = array(
    'comments' =>Comment::all(),
    'foodtypes'  =>  FoodType::all(),
    'foods'   =>  DB::table('food_items')
                ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
                ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
                ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id')
                ->get(),
    'shops' =>Shop::find(session()->get('shop_id')),
    'orderTs' => DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('order_tems','order_tems.food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','order_tems.quantity','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
    
);
        return view('users.comment' )->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $comm = new comment;
            $email = $request->emailaddress;
            $comment = $request->comment;
            $shop_id = $request->shop_id;
        $comm->emailaddress = $email;
        $comm->comment = $comment;
        $comm->shop_id = $shop_id;

        if($comm->save()){
            return $comm;
        }
       else{
           return response()->json($request);

       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function show(Comment $comment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function edit(Comment $comment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Comment $comment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Comment  $comment
     * @return \Illuminate\Http\Response
     */
    public function destroy(Comment $comment)
    {
        //
    }
}
