<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class FoodSize extends Model
{
    protected $fillable = [
    'size_name' , 'food_items_id' , 'price' , 
    ];

    public function fooditem()
    {
        return $this->belongTo('App\FoodItem' , 'food_items_id');
    }

     public function ordertemp(){
        return $this->belongsTo('App\OrderTemp','Food_sizes_id');
    }
     public function order_items(){
        return $this->belongsTo('App\Order_items','Food_sizes_id');
    }
}
