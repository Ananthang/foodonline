<?php

namespace App;
use Illuminate\Notifications\Notifiable;
use Illuminate\Database\Eloquent\Model;

class Customer extends Model
{

use Notifiable;

    protected $fillable = [
    'customername' ,  'address' , 'email' , 'phoneno' , 'city' , 'shop_id' ,
];

   public function order()
    {
        return $this->belongsToMany('App\Order','customer_id');
    }
    public function shop(){
        return $this->belonsToMany('App\Shop','id');
    }
}
