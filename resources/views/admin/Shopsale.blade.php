@extends('layouts.homenav')

@section('title','Shop Sale ')

@section('content')
<br>
<br>
<br>


   

        <div class="card card-cascade">

            <p>
             <table class="table table-striped table-bordered table-hover " id="TableId">
                <thead class="">
                    <tr class="">
                        <th>Sales Id</th>
                        <th>Customer Name</th>
                        <th>Customer Phone Number</th>
                        <th>Price(Rs.)</th>
                        <th>Delivered Date</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($shopSales as $shopSale)

                    <tr>
                        <td>{{$shopSale->id}}</td>
                        
                        <td>{{$shopSale->customername}}</td>
                        <td>
                            {{$shopSale->phoneno}}
                        </td>
                        <td data-id="{{$shopSale->total}}" class="pricesales">{{$shopSale->total}}</td>
                        <td>{{$shopSale->created_at}}</td>


                    </tr>
                    @endforeach
                </tbody>

            </table>
        </p>
        <table class="table table-striped table-bordered table-hover">
          <thead class="">
            <tr class="">
              <th>
                <strong>Total (Rs.)</strong>
              </th>
              <th id="totalOrderSale"></th>

            </tr>
          </thead>

        </table>
        </div>
        
   
<!--/main body page-->
@endsection