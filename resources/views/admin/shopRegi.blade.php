<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shop Registration</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/font.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="/css/mdb.min.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="/css/style.css" rel="stylesheet">
    <!--icon Design -->
    <link href="/css/stylelogin.css" rel="stylesheet">
      <link href="/css/customboostrap.css" rel="stylesheet">
    <link rel="stylesheet" href="css/icons/icomoon/styles.css">

</head>

<body>

    <!-- Start your project here-->
    <!-- SideNav slide-out button -->
    <header>

        @foreach(['danger','warning'] as $msg) @if(session()->has('alert-' . $msg ))

        <div>
            <p class="alert alert-{{ $msg }}">
                {{session()->get('alert-' . $msg)}};
            </p>
        </div>


        @endif @endforeach @if(session('shop'))
        <div>
            <p class="alert alert-success">
                {{session('shop')}}
            </p>
        </div>
        @endif
    </header>
    <!--/. Sidebar navigation -->
    <!-- /Start your project here-->
    <main>

        <!--main body page-->



        <div class="row">

            <div class="col-md-3">

            </div>
            <div class="col-md-6">
                <br>
                <br>
                <br>
                <div class="card card cascade">
                    <div class="view overlay hm-white-slight">
                        <div>
                            <form action="anan" method="POST" enctype="multipart/form-data" id="formReg">
                                {{csrf_field() }}
                               
                                <div class="card-header" id="hea">
                                    <p class="h3 text-center mb-4 thicker">Registration Form</p>
                                </div>
                                <i class="fa fa-shopping-basket  prefix grey-text"></i>

                                <label for="orangeForm-shopname">Shopname</label>
                                <div class="form-group">
                                    <input type="text" id="orangeForm-shopname" name="shopname" class="form-control">
                                </div>
                                <br>

                                <i class="fa fa-user prefix grey-text"></i>
                                <label for="orangeForm-address">Address</label>
                                <div class="form-group">
                                    <input type="text" id="orangeForm-address" name="address" class="form-control">
                                </div>
                                <br>

                                <i class="fa fa-envelope prefix grey-text"></i>
                                <label for="orangeForm-email">Email Address</label>
                                <div class="form-group">
                                    <input type="text" id="orangeForm-email" name="email" type="email" class="form-control">
                                </div>
                                <br>

                                <i class="fa fa-phone prefix grey-text"></i>

                                <label for="orangeForm-phoneno">Phone Number</label>
                                <div class="form-group">
                                    <input type="text" id="orangeForm-phoneno" name="phoneno" class="form-control">
                                </div>
                                <br>


                                <label for="orangeForm-shop_description">Shop Description </label>
                                <div class="form-group">
                                    <textarea type="textarea" id="orangeForm-shop_description" name="shop_description" class="form-control"></textarea>
                                </div>
                                <br>
                                

                                <i class="fa fa-image prefix grey-text"></i>
                                <div class="custom-file">
                                    <input type="file" name="shoplogo" class="custom-file-input" id="customFile">
                                    <label class="custom-file-label" for="customFile">Choose Shop Logo</label>
                                </div>
                               
                               
                                <br>
                                <br>
                                


                                <div class="text-center">
                                    <button class="btn btn-primary btn-lg" name="submit" type="submit">Create New Account</button>
                                </div>

                            </form>
                            <!-- Form register -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br> @if ($errors->any()) @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
                @endforeach @endif
            </div>
        </div>

        <!--/main body page-->
        </div>
        </div>
    </main>
    <!--/main-->
    <footer>


    </footer>
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script>
    </script>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrapc.min.js"></script>
    <!-- MDB core JavaScript -->
    <script type="text/javascript" src="js/mdb.min.js"></script>
    <script src="js/custom.js"></script>
    <script>
        /* $(document).ready(function() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                         $('.dropp').slideUp("slow");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      $(".dropper").click(function() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          $(".dropp").slideToggle("slow");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                      });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  /*  function drop() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                       $("#dropp").slideUp("slow");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                        alert("ahsfdad");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                             }*/
        $("#slidenava").sideNav();
        // SideNav Options
        $('#slidenava').sideNav({
            edge: 'right', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });

        // Show sideNav
        $('#slidenava-item').sideNav('hide');
        // Hide sideNav

        $('#slidenava-item').sideNav('show');
    </script>

</body>

</html>