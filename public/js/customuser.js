$('.carousel').carousel();


var count = 0;

count = sessionStorage.getItem("count");







$(document).ready(function() {
    var id = window.navigator.userAgent.replace(/\D+/g, '');
    $(function() {
        $("#slider1").sliderResponsive({
            fadeSpeed: 5000,

        });


    });



    var controller = new ScrollMagic.Controller();


    $('.welcomeCard').each(function() {

        var ourscene = new ScrollMagic.Scene({
                triggerElement: this.children[1],
                triggerHook: 0.6,
                duration: 1000,
                easing: "linear"
            })
            .setClassToggle(this, 'fade-in')

        .addTo(controller);
    });

    var controller1 = new ScrollMagic.Controller();

    var ourscreneImg = new ScrollMagic.Scene({
        triggerElement: '#adoutusIma',
    }).on('start', function() {
        $('#adoutusIma').show().addClass('animated bounceInLeft');
    })

    .addTo(controller1);




    $(".back").hide();
    total();
    $('table#commentTableId').DataTable({
        "info": false,

        //"scrollY": true,	
        //"scrollCollapse": true,
        //fixedHeader: false,
        fixedColumns: true
    });





    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });
    // Insert AddCart IN OrderTemp
    $(document).on('click', 'a.addcard', function() {
        var product_id = $(this).data('id');
        var url = "/order/addcart/temp/";
        var quantity = 1;
        var addcart = "addcart";
        var tagId = addcart.concat(product_id);
        var buttonid = ("#addcart".concat(product_id));

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                food_sizes_id: product_id,
                code_id: id.toString(),
                quantity: quantity,

            },
            success: function(data) {

                count++;

                $("#butNav").show(3000);
                document.getElementById("count").innerHTML = count;
                sessionStorage.setItem("count", count);
                $(buttonid).removeClass('show').addClass('hide');

                $('.addCartList').load(location.href + " .addCartList");
                total();


            },
            error: function(data) {

                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',

                })

            }
        });
    });


    //Addcart Update Function Here
    $(document).on('click', 'a.updateAddCart', function() {
        var product_id = $(this).data('id');
        var url = "/order/addcart/temp/update";

        var quantity = $("#quantity".concat(product_id)).val();

        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                food_sizes_id: product_id,
                code_id: id.toString(),
                quantity: quantity,

            },
            success: function(data) {

                $('.addCartList').load(location.href + " .addCartList");
                total();

                $('.addCartList').load(location.href + " .addCartList");
                total();
                swal({
                    position: 'top-end',
                    type: 'success',
                    title: 'Thank! Your Addcart is successfully updated',
                    showConfirmButton: false,
                    timer: 1500
                });

            },
            error: function(data) {

                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong!',

                })

            }
        });

    });

    //Add cart Delete .......
    $(document).on('click', 'a.deleteAddCart ', function() {

        var product_id = $(this).data('id');
        var url = "/order/addcart/temp/delete/" + product_id;

        var quantity = $("#quantity".concat(product_id)).val();
        swal({
            title: 'Are you sure?',
            text: "You want Remove this item ",
            type: 'warning',
            showCancelButton: true,
            confirmButtonColor: '#3085d6',
            cancelButtonColor: '#d33',
            confirmButtonText: 'Yes, Remove it!'
        }).then((result) => {
            if (result.value) {
                $.ajax({
                    dataType: 'json',
                    type: 'GET',
                    url: url,
                    data: {
                        'id': product_id
                    },
                    success: function(data) {

                        $('.addCartList').load(location.href + " .addCartList");
                        total();

                        swal(
                            'Deleted!',
                            'Your file has been removed.',
                            'success'
                        )

                    },
                    error: function(data) {

                        swal({
                            type: 'error',
                            title: 'Oops...',
                            text: 'Something went wrong!',

                        })


                    }
                });

            }
        })


    });


    //Comment Ajax
    $(document).on('click', 'a.commentSubmit', function() {

        var email = $('#commentEmail').val();
        var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
        if (filter.test(email)) {
            var commentT = $('#commentText').val();
            var shop_id = $('#commentShop_id').val();
            var url = "/comment";



            $.ajax({
                dataType: 'json',
                type: 'POST',
                url: url,
                data: {
                    emailaddress: email,
                    comment: commentT,
                    shop_id: shop_id
                },
                success: function(data) {


                    $("#commentTable").load(location.href + ' #commentTable');
                    swal({
                        position: 'top-end',
                        type: 'success',
                        title: 'Thank! For Your valuable comment',
                        showConfirmButton: false,
                        timer: 3000
                    })
                },
                error: function(data) {
                    swal({
                        type: 'error',
                        title: 'Oops...',
                        text: 'Something went wrong! please comment again ',
                        footer: 'Sorry....',
                    })

                }
            });

        } else {
            swal({
                type: 'error',
                title: 'Oops...',
                text: 'Something went wrong! please comment again ',
                footer: 'Sorry....',
                timer: 3000
            })
        }

    }); //comment ajax end


    //Order Ajax start here
    //Order Ajax takeaway start here

    $(document).on('click', 'a.orderplacetakeaway', function() {

        var name = $('#namet').val();
        var phonenumber = $('#phonenumbert').val();
        var emailaddress = $('#emailaddresst').val();
        var order_type = "order now";
        var delivery_type = $(this).data('id');
        if ($('#deliverylatertake').is(':checked')) {
            var date = $('#datetimepickertake').val();
            order_type = "order later";

        } else {
            var date = moment().format("YYYY-MM-DD HH:mm:ss");
        }

        var url = "/order";




        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                name: name,
                phoneno: phonenumber,
                emailaddress: emailaddress,
                orderdate: date,
                order_type: order_type,
                id: id,
                delivery_type: delivery_type,
            },
            success: function(data) {



                swal({
                    position: 'top-end',
                    type: 'success',
                    title: 'Thank! Your Order is successfully placed',
                    showConfirmButton: false,
                    timer: 3000
                });
                $('#homeback').removeClass('hide').addClass('show');
                $('#submit').removeClass('show').addClass('hide');
                $('#reset').removeClass('show').addClass('hide');
            },
            error: function(data) {

                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! pleace check your phone number and email address please try again',
                    footer: 'Sorry....',
                })

            }
        });



    });
    //Order Ajax takeaway end here
    //Order Ajax door delivery start here

    $(document).on('click', 'a.orderplacedoordelivery', function() {

        var name = $('#nameD').val();
        var phonenumber = $('#phonenumberD').val();
        var emailaddress = $('#emailaddressD').val();
        var address = $('#addressD').val();
        var city = $('#cityD').val();
        var order_type = "order now";
        var delivery_type = $(this).data('id');
        if ($('#deliverylaterD').is(':checked')) {
            var date = $('#datetimepickerdoor').val();
            order_type = "order later";
        } else {
            var date = moment().format("YYYY-MM-DD HH:mm:ss");
        }

        var url = "/order";




        $.ajax({
            dataType: 'json',
            type: 'POST',
            url: url,
            data: {
                name: name,
                phoneno: phonenumber,
                emailaddress: emailaddress,
                orderdate: date,
                address: address,
                order_type: order_type,
                city: city,
                id: id,
                delivery_type: delivery_type
            },
            success: function(data) {



                swal({
                    position: 'top-end',
                    type: 'success',
                    title: 'Thank! Your Order is successfully placed',
                    showConfirmButton: false,
                    timer: 3000
                });
                $('#homeback').removeClass('hide').addClass('show');
                $('#submit').removeClass('show').addClass('hide');
                $('#reset').removeClass('show').addClass('hide');
            },
            error: function(data) {
                console.log(data);
                swal({
                    type: 'error',
                    title: 'Oops...',
                    text: 'Something went wrong! pleace check your phone number and email address please try again',
                    footer: 'Sorry....',
                })

            }
        });



    });
    //Order Ajax doordelivery end here
    // order ajax end

    //doucument ready function end
});



jQuery.validator.setDefaults({
    debug: true,
    success: "valid"
});

$('#doordeliveryform').validate({
    rules: {
        nameD: "required",
        phonenumberD: {
            required: true,
            digits: true,
            minlength: 10,
            maxlength: 10,


        },
        emailaddressD: {
            required: true,
            email: true
        },
        addressD: "required",
        cityD: "required"
    },
    massege: {
        nameD: "Please enter your name",
        phonenumberD: {

            required: "Please enter your phone number",
            minlength: "It want to be 10 digits",
        },
        emailaddressD: {
            required: "Please enter your email address",
            email: "Please correct your email address (Eg:********@****.com)"
        },
        addressD: "Please enter your address",
        cityD: "Please enter your city"
    },
});
$('#takeawayform').validate({
    rules: {
        namet: "required",
        phonenumbert: {
            required: true,
            digits: true,
            minlength: 10,
            maxlength: 10,


        },
        emailaddresst: {
            required: true,
            email: true
        },

    },
    massege: {
        namet: "Please enter your name",
        phonenumbert: {

            required: "Please enter your phone number",
            minlength: "It want to be 10 digits",
        },
        emailaddresst: {
            required: "Please enter your email address",
            email: "Please correct your email address (Eg:********@****.com)"
        },

    },
});


function moreInfo(id) {
    var Bac1 = "back";
    var i = "#";
    var Bac = i.concat(Bac1, id);
    var Front1 = "front";
    var Front = i.concat(Front1, id);
    $(Front).hide(100);
    $(Bac).show(100);

};

$(".quantity").keyup(
    function() {
        var id = $(this).data('id');

        var pr = "price";
        var i = "#";
        var PriceId = i.concat(pr, id);
        var i = "#";
        var QuaPriId = pr.concat(id);
        var Price = $(PriceId).data('id');

        var valu = $(this).val();
        if (isPositiveInteger(valu)) {
            $(this).removeClass("form-control-error");
            $(this).addClass("form-control-correct");
            var QuaPri = parseInt(Price) * parseInt(valu);
            // var Qu = valu.toString();
            var Qu = QuaPri.toString();
            document.getElementById(QuaPriId).innerHTML = Qu;
            total();
        } else {
            $(this).removeClass("form-control-correct");
            $(this).addClass("form-control-error");
        }
    }

);


function total() {

    var total = 0;

    $('.orderTPrice').each(function() {
        total += parseFloat(this.innerHTML);
        var val = $(this).text();

    });

    $('#total').text(total);


}


function isPositiveInteger(s) {
    var i = +s;
    if (i < 0) return false;
    if (i != ~~i) return false;
    return true;
}
/*
function validateEmailAddress() {
    var filter = /^([\w-\.]+)@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.)|(([\w-]+\.)+))([a-zA-Z]{2,4}|[0-9]{1,3})(\]?)$/;
    var email = document.getElementById("commentEmail").value;
    console.log(email);
    if (filter.test(email)) {
        console.log("correct");
    } else {
        console.log("errorr");
    }
}*/
function comment() {

    $('#comment').html(html);
}
//welcome page js end Here

//order js start Here
function checkOut() {
    window.location.href = '/order/addcart/temp';
}

function doordeliveryfunction() {
    $('#v-pills-takeaway-tab').hide(3000);
    $('#v-pills-tabContent').removeClass('hide').addClass('show');
}

function takeawayfunction() {
    $('#v-pills-doordelivery-tab').hide(3000);
    $('#v-pills-tabContent').removeClass('hide').addClass('show');
}
$('.deliverynow').click(function() {
    if ($(this).is(':checked')) {
        $('.deliverylaterId').hide(3000);
    }
});
$('.deliverylater').click(function() {
    if ($(this).is(':checked')) {
        $('.deliverynowId').hide(3000);
        $('.orderPickers').removeClass("hide").addClass("show");
    }
});
window.setInterval(function() {
    $('.datetimepicker').daterangepicker({
        "singleDatePicker": true,
        "timePicker": true,
        "timePicker24Hour": true,
        "autoApply": true,
        "dateLimit": {
            "days": 7
        },
        "locale": {
            "format": 'YYYY-MM-DD HH:mm:ss',
            "separator": " - ",
            "applyLabel": "Apply",
            "cancelLabel": "Cancel",
            "fromLabel": "From",
            "toLabel": "To",

            "customRangeLabel": "Custom",
            "weekLabel": "W",
            "daysOfWeek": [
                "Su",
                "Mo",
                "Tu",
                "We",
                "Th",
                "Fr",
                "Sa"
            ],
            "monthNames": [
                "January",
                "February",
                "March",
                "April",
                "May",
                "June",
                "July",
                "August",
                "September",
                "October",
                "November",
                "December"
            ],
            "firstDay": 1
        },
        "minDate": moment(),
        "startDate": "04/05/2018",
        "endDate": "04/11/2018"
    }, function(start, end, label) {

    });
}, 10000);


//order js end here



$('#searchBoxId').keyup(function() {

    $.ajaxSetup({
        headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')

        }

    });

    var search = $('#searchBoxId').val();
    var data_id = $('#searchBoxId').data('id');


    if (data_id == "home") {
        var url = "fooditems/search/" + search;

        window.location.href = url;
    } else {
        var url = search;
        window.location.href = url;

    }





});

$('#searchBoxId').mouseenter(function() {
    $('#searchbody').addClass('hide').removeClass('show');
    $('#searchResult').addClass('show').removeClass('hide');
});
$('#searchBoxId').mouseleave(function() {
    $('#searchbody').addClass('show').removeClass('hide');
    $('#searchResult').addClass('hide').removeClass('show');
});

$('#moreInfosearch').click(function() {
    var moreId = $(this).data('id');

    var url = "more/" + moreId;


    window.location.href = url;

});