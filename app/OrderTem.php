<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class OrderTem extends Model
{
    protected $fillable = [
       'food_sizes_id' ,'code_id','quantity'
    ];

    public function foodsize(){
        return $this->belongsTo('App\FoodSize','food_sizes_id');
    }
}
