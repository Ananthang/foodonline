<?php

namespace App\Http\Controllers;

use App\Auth;
use App\User;
use App\FoodType;
use App\Shop;
use App\FoodItem;
use App\FoodSize;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;


class SearchController extends Controller
{
    public function index($search){
        $shop_id = session('shop_id');
         $data = array(
        'searches' => FoodItem::search($search)->get(),
        'shops' => Shop::find($shop_id),
    'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get()
                                );
    
      
    return view('users.search')->with($data);

    }


    public function searchId($searchId){
        $shop_id = session('shop_id');
         $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' ,'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where([['food_types.shop_id','=',session()->get('shop_id')],['food_sizes.food_items_id','=',$searchId]])
            ->get(),
                'shops' =>Shop::find(session()->get('shop_id')),
);

return view('users.details')->with($data);
   
    }
}
