<?php

namespace App\Http\Controllers;
use App\Auth;
use App\User;
use App\FoodType;
use App\Shop;
use App\FoodItem;
use App\FoodSize;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;

class FoodItemController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if (session()->has('shop_id')){
        $shop_id = session('shop_id');
        $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
                'shops' =>Shop::find(session()->get('shop_id')),
);
        return view('admin.food_item.items' )->with($data);}

        else{
            return Redirect::to('/admin');
        }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       

        if($request->sizename1 != "" && $request->sizename2 != "")
        {
             $validator = Validator::make($request->all(),[
                        
                
                'foodname' => 'required' ,
                'foodImg' => 'required' ,
                'description' => 'required' ,
                'food_types_id' => 'required' ,
                'sizename' => 'required' ,
                'sizename1' => 'required' ,
                'sizename2' => 'required' ,
                'price' => 'required',
                'price1' => 'required',
                'price2' => 'required'

                ]);

                    
                
                if($request->hasFile('foodImg')){
                        
                     $foodname = $request->foodname;
                    $foodImg = $request->file('foodImg');
                    $description = $request->description;
                    $food_types_id = $request->food_types_id;
                    $sizename = $request->sizename;
                    $sizename1 = $request->sizename1;
                    $sizename2 = $request->sizename2;
                    $price = $request->price;
                    $price1 = $request->price1;
                    $price2 = $request->price2;

                    $filename = $foodname.'.'.$foodImg->getClientOriginalExtension();
                    $request->foodImg->storeAS('public/upload/foodImage',$filename);

                    $foodItem = new FoodItem;
                        
                    $foodItem->foodname = $foodname;
                    $foodItem->foodImg = $filename;
                    $foodItem->description = $description;
                    $foodItem->food_types_id = $food_types_id;
                            
                if($foodItem->save())
                    {
                        $size = new FoodSize ;
                        $users= DB::table('food_items')
                        ->where('foodname', '=', $foodname)
                        ->select('id')
                        ->latest()
                        ->get();
                        foreach($users as $user){
                            $size->food_items_id = $user->id;
                        }
                        $size->size_name = ucfirst($sizename) ;
                        $size->price = $price;

                      
                                    

                        if($size->save()){

                            $size1 = new FoodSize ;
                            $users= DB::table('food_items')
                            ->where('foodname', '=', $foodname)
                            ->select('id')
                            ->latest()
                            ->get();
                            foreach($users as $user){
                                $size1->food_items_id = $user->id;
                            }
                            $size1->size_name = ucfirst($sizename1) ;
                            $size1->price = $price1;

                            if($size1->save())
                            {

                                $size2 = new FoodSize ;
                                $users= DB::table('food_items')
                                ->where('foodname', '=', $foodname)
                                ->select('id')
                                ->latest()
                                ->get();
                                foreach($users as $user){
                                    $size2->food_items_id = $user->id;
                                }
                                $size2->size_name = ucfirst($sizename2) ;
                                $size2->price = $price2;


                                if($size2->save())
                                {
                                    session()->flash('user', 'your food item is insert successfully' );
                                                                        
                                    return redirect('/admin/home/fooditem');
                                }
                                else
                                {
                                     session()->flash('user', 'your food item is not  insert completly successfully' );
                                return 'your food item is not  insert completly successfully';
                                }
                            
                            }
                            else
                            {
                                session()->flash('user', 'your food item is not  insert completly successfully' );
                                return 'your food item is not  insert completly successfully';
                            }

                            
                        }
                        else
                        {
                            session()->flash('user', 'your food item is not  insert completly successfully' );
                            return back()->withInput();
                        }
                }

                else
                {
                    session()->flash('user', 'your food item is not  insert completly successfully' );
                     return back()->withInput();
                }
            }
            else
            {
                session()->flash('user', 'your food Image not exists' );                        
                 return back()->withInput();
            }
        
        }
        elseif($request->has('sizename1') && $request->sizename2 == "")
        {
             $validator = Validator::make($request->all(),[
                        
                
                'foodname' => 'required' ,
                'foodImg' => 'required' ,
                'description' => 'required' ,
                'food_types_id' => 'required' ,
                'sizename' => 'required' ,
                'sizename1' => 'required' ,
                'price' => 'required',
                'price1' => 'required'

                ]);

                    
                
                if($request->hasFile('foodImg')){
                        
                     $foodname = $request->foodname;
                    $foodImg = $request->file('foodImg');
                    $description = $request->description;
                    $food_types_id = $request->food_types_id;
                    $sizename = $request->sizename;
                    $sizename1 = $request->sizename1;
                    $price = $request->price;
                    $price1 = $request->price1;

                    $filename = $foodname.'.'.$foodImg->getClientOriginalExtension();
                    $request->foodImg->storeAS('public/upload/foodImage',$filename);

                    $foodItem = new FoodItem;
                        
                    $foodItem->foodname = $foodname;
                    $foodItem->foodImg = $filename;
                    $foodItem->description = $description;
                    $foodItem->food_types_id = $food_types_id;
                            
                if($foodItem->save())
                    {
                        $size = new FoodSize ;
                        $users= DB::table('food_items')
                        ->where('foodname', '=', $foodname)
                        ->select('id')
                        ->latest()
                        ->get();
                        foreach($users as $user){
                            $size->food_items_id = $user->id;
                        }
                        $size->size_name = ucfirst($sizename) ;
                        $size->price = $price;

                      
                                    

                        if($size->save()){

                            $size1 = new FoodSize ;
                            $users= DB::table('food_items')
                            ->where('foodname', '=', $foodname)
                            ->select('id')
                            ->latest()
                            ->get();
                            foreach($users as $user){
                                $size1->food_items_id = $user->id;
                            }
                            $size1->size_name = ucfirst($sizename1) ;
                            $size1->price = $price1;

                            if($size1->save())
                            {
                            session()->flash('user', 'your food item is insert successfully' );
                                    
                            return redirect('/admin/home/fooditem');
                            }
                            else
                            {
                                session()->flash('user', 'your food item is not  insert completly successfully' );
                                return 'your food item is not  insert completly successfully';
                            }

                            
                        }
                        else
                        {
                            session()->flash('user', 'your food item is not  insert completly successfully' );
                            return 'your food item is not  insert completly successfully';
                        }
                }

                else
                {
                    session()->flash('user', 'your food item is not  insert completly successfully' );
                    return 'your food item is not  insert completly successfully';
                }
            }
            else
            {
                session()->flash('user', 'your food Image not exists' );                        
                return 'your food Image not exists';
            }
        
        }

        else{
                $validator = Validator::make($request->all(),[
                        
                
                'foodname' => 'required' ,
                'foodImg' => 'required' ,
                'description' => 'required' ,
                'food_types_id' => 'required' ,
                'sizename' => 'required' ,
                'price' => 'required'

                ]);

                    
                
                if($request->hasFile('foodImg')){
                        
                     $foodname = $request->foodname;
                    $foodImg = $request->file('foodImg');
                    $description = $request->description;
                    $food_types_id = $request->food_types_id;
                    $sizename = $request->sizename;
                    $price = $request->price;

                    $filename = $foodname.'.'.$foodImg->getClientOriginalExtension();
                    $request->foodImg->storeAS('public/upload/foodImage',$filename);

                    $foodItem = new FoodItem;
                        
                    $foodItem->foodname = $foodname;
                    $foodItem->foodImg = $filename;
                $foodItem->description = $description;
                $foodItem->food_types_id = $food_types_id;
                            
                if($foodItem->save())
                    {
                        $size = new FoodSize ;
                        $size->size_name = ucfirst($sizename) ;
                        $size->price = $price;

                    $users= DB::table('food_items')
                        ->where('foodname', '=', $foodname)
                        ->select('id')
                        ->latest()
                        ->get();
                        foreach($users as $user){
                            $size->food_items_id = $user->id;
                        }
                                    

                        if($size->save()){
                            session()->flash('user', 'your food item is insert successfully' );
                                    
                            return redirect('/admin/home/fooditem');
                        }
                        else
                        {
                            session()->flash('user', 'your food item is not  insert completly successfully' );
                            return 'your food item is not  insert completly successfully';
                        }
                }

                else
                {
                    session()->flash('user', 'your food item is not  insert completly successfully' );
                    return 'your food item is not  insert completly successfully';
                }
            }
            else
            {
                session()->flash('user', 'your food Image not exists' );                        
                return 'your food Image not exists';
            }
        
        }
    
        
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodItem  $foodItem
     * @return \Illuminate\Http\Response
     */
    public function show(FoodItem $foodItem)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodItem  $foodItem
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        if(session()->has('shop_id')){
            $size =   FoodSize::find($id);
            $food = FoodItem::find($size->food_items_id);
            $type = FoodType::find($food->food_types_id);
         $data = array(
           //  'size' =>Foodsize::find($id),
  // 'foodtypes'  =>  FoodType::all(),
  // 'foods'   =>  DB::table('food_items')->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')->join('food_types' , 'food_items.food_types_id','=','food_types.id')->select('food_items.id','food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price')->get(),
  //  'shops' =>Shop::find(session()->get('shop_id')),
     //'item' => FoodItem::find($id),
     'shops' =>Shop::find(session()->get('shop_id')),
        'size' => FoodSize::find($id),
        'food' => FoodItem::find($size->food_items_id),
      'type' => FoodType::find($food->food_types_id),
      'foodtypes' =>FoodType::all(),
      
        );
       
        return view('admin.food_item.edit')->with($data);}
        else{
            return Redirect::to('/admin');
        }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodItem  $foodItem
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request , $id)
    {
        if(session()->has('shop_id')){

        
        if( $request->hasFile( 'foodImage' ) ) {

           $size =   FoodSize::find($id);
            $food = FoodItem::find($size->food_items_id);
            $type = FoodType::find($food->food_types_id);
            //969BE242-C8EC-4ABC-B328-1FE53A65D389
             // Storage::delete($food_item->name);


                 $file = $request->foodImage->getClientOriginalName();

                $request->foodImage->storeAS('public/upload/food',$file);


             $food->foodname = $request->foodname;
             $food->foodImg = $file;
             $food->food_types_id = $request->food_types_id;
             if($food->save()){ 
                 $sizename = $request->sizename ;
                $size->size_name = ucfirst($sizename);
                $size->price = $request->price;
                if($size->save()){
                    $request->session()->flash('update' , 'Your update is successfully done');
                    return redirect('/admin/home/fooditem');
                }
             }
             else{
                     $request->session()->flash('update' , 'Your update is not successfully done');
                     return back()->withInput();
             }

            }
            else{
                     $request->session()->flash('update' , 'Your update is not successfully done');
                     return back()->withInput();
             }



}
       else{
           return Redirect::to('/admin');
       }



    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodItem  $foodItem
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        if(session()->has('shop_id')){

          $size =   FoodSize::find($id);
       $size->delete();


       session()->flash('messageDele','Data is Deleted ');
        return redirect('/admin/home/fooditem');}
       else{
           return Redirect::to('/admin');
       }
    }
}