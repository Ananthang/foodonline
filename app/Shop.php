<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Shop extends Model
{
     protected $fillable = [
       'shopname' , 'shoplogo' , 'address' , 'email' , 'phoneno', 'shop_description','facebookAddress'
    ];

    public function user(){
        return $this->hasMany('App\User','shop_id');
    }
    public function customer(){
        return $this->hasMany('App\Customer','shop_id');
    }
    public function Special(){
        return $this->hasMany('App\Special','shop_id');
    }
    public function offer(){
        return $this->hasMany('App\Offer','shop_id');
    }
    public function ShopSale(){
        return $this->hasMany('App\ShopSale','shop_id');
    }
}
