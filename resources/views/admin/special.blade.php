@extends('layouts.homenav')


@section('content')
  <br>
                    <br>
                    <br>
    <ul class="nav nav-tabs nav-justified blue" role="tablist">
    <li class="nav-item ">
        <a class="nav-link active" data-toggle="tab" href="#fooditems" role="tab">
            Food Items
        </a>
    </li>
    <li class="nav-item ">
        <a class="nav-link" data-toggle="tab" href="#currentSpecial" role="tab">
            Current Special Food Items
        </a>
    </li>
</ul>
<!-- /tab panel list-->
<!--tap panel content -->
<div class="tab-content card">
    <!--tap panel content show description-->
    <div class="tab-pane fade  show active" id="fooditems" role="tabpanel">
        <br>
<div class="alert alert-info">
    <p>
        This Special Items are can be famous food the Food It or most selling Food Items or Today Special Items,

    </p>
    <p>
        If Your Want to Add New Special Food Item Then You Want Create that Food Item.
    </p>
</div>
          <p>
             <table class="table table-striped table-bordered table-hover specialupdate" id="Table1Id">
                <thead class="">
                    <tr class="">
                        <th>Food Item id</th>
                        <th>Food Name</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Size</th>
                        <th>Price (Rs.)</th>
                        <th>Add Special Items List</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($foods as $food)

                    <tr>
                        <td>{{$food->id}}</td>
                        <td>{{$food->foodname}}</td>
                        <td>
                            <img src="/storage/upload/foodImage/{{$food->foodImg}}" id="showImage" class="img-fluid" alt="{{$food->foodname}}"></img>
                        </td>
                        <td>{{$food->description}}</td>
                        <td>{{$food->typename}}</td>
                        <td>{{$food->size_name}}</td>
                        <td >
                         {{$food->price}} 
                        </td>
                        <td>
                        <a class="btn btn-success addSpecialItem" data-id = "{{ $food->id }}">Add Special Items List</a>
                        </td>
                        
                        


                    </tr>
                    @endforeach
                </tbody>

            </table>
        </p>

    </div>

    <div class="tab-pane fade" id="currentSpecial" role="tabpanel">
        <br>
        <p>
             <table class="table table-striped table-bordered table-hover specialupdate1 " id="TableId">
                <thead class="">
                    <tr class="">
                        <th>Food Item id</th>
                        <th>Food Name</th>
                        <th>Image</th>
                        <th>Description</th>
                        <th>Type</th>
                        <th>Size</th>
                       
                        <th>Price</th>
                        <th>Remove Special Items List</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($specials as $special)

                    <tr>
                        <td>{{$special->id}}</td>
                        <td>{{$special->foodname}}</td>
                        <td>
                            <img src="/storage/upload/foodImage/{{$special->foodImg}}" id="showImage" class="img-fluid" alt="{{$special->foodname}}"></img>
                        </td>
                        <td>{{$special->description}}</td>
                        <td>{{$special->typename}}</td>
                        <td>{{$special->size_name}}</td>
                        <td>
                         {{$special->price}}  
                        </td>
                        <td>
                            <a class="btn btn-warning removeSpecialItem" data-id = "{{$special->id}}">Remove Special Food Item</a>
                        </td>
                        


                    </tr>
                    @endforeach
                </tbody>

            </table>
        </p>

        <!--tap panel content show description-->
    </div>
             
@endsection