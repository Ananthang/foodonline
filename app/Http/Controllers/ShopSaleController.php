<?php

namespace App\Http\Controllers;

use App\ShopSale;
use App\Shop;
use App\Order;
use App\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use Image;
use Illuminate\Support\Facades\Redirect;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Carbon\Carbon;

class ShopSaleController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         if(session()->has('shop_id')){
 
                $data = array('shops' =>Shop::find(session()->get('shop_id')),
                            'sales' => ShopSale::where('shop_id','=',session()->get('shop_id'))->get(),
                            'shopSales' => DB::table('orders')
                                        ->join('customers','customers.id','=','orders.customer_id')
                                        ->join('shop_sales','shop_sales.order_id','=','orders.id')
                                        ->select('customers.customername','shop_sales.total','shop_sales.created_at','shop_sales.id','customers.phoneno')->get(),
                );
            return view ('admin.Shopsale')->with($data);
        }
        else{
                    return Redirect::to('/admin');
            }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\ShopSale  $shopSale
     * @return \Illuminate\Http\Response
     */
    public function show(ShopSale $shopSale)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\ShopSale  $shopSale
     * @return \Illuminate\Http\Response
     */
    public function edit(ShopSale $shopSale)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\ShopSale  $shopSale
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ShopSale $shopSale)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\ShopSale  $shopSale
     * @return \Illuminate\Http\Response
     */
    public function destroy(ShopSale $shopSale)
    {
        //
    }
}
