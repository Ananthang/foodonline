@extends('layouts.homenav')

@section('title','Food Items Edit' )

@section('content') @if(session()->has('message'))
<p class="alert alert-success"> {{session()->get('message')}} </p>

@endif

<br>
<br>
<br>
<div class="row">
    <div class="col-md-3"></div>
    <div class="col-md-6">
        <div class="card card-cascade">



            <div class="view overlay hm-white-slight">
                <img src="/storage/upload/foodImage/{{$food->foodImg}}" id="profileImg" class="img-rounded img-responsive " alt="">
                <a class="waves-effect waves-light"></a>
                <div class="mask"></div>
            </div>


            <div class="card-body">
            <form action="/admin/home/fooditem/{{$size->id}}" method="POST" enctype="multipart/form-data" id="formReg">
                    {{csrf_field() }} {{method_field('PUT')}}
                    <p class="h3 text-center mb-4 thicker">Update Food Items</p>

                    <input type="hidden" name="id" value="{{$size->id}}">
                    <input type="hidden" name="food_item_id" value="{{$food->id}}"> 
                    <input type="hidden" name="types_id" value="{{$type->id}}"> 

                    <label for="orangeForm-foodname"> Food Name</label>
                    <div class="form-group">
                        <input type="text" id="orangeForm-foodname" name="foodname" class="form-control">
                    </div>


                    <label for="orangeForm-foodImage">Image</label>
                    <div class="form-group">
                        <input type="file" id="orangeForm-name" name="foodImage" class="form-control">
                    </div>


                    <label for="orangeForm-food_description"> Food_Description</label>
                    <div class="form-group">
                        <textarea type="text" id="orangeForm-food_description" name="food_description" class="form-control" placeholder="Description here"></textarea>
                    </div>


                    <div class="form-group">
                        <label for="sel1">Food Type</label>
                        <select class="form-control" name="food_types_id" id="sel1">
                            @foreach ($foodtypes as $foodtype)
                            <option value="{{$foodtype['id']}}">{{$foodtype['typename']}}</option>
                            @endforeach
                        </select>
                    </div>



                    <label for="orangeForm-sizename">size</label>
                    <div class="form-group">
                        <input type="text" id="orangeForm-sizename" name="sizename" class="form-control" onclick="toastr.info('Hi! If you have more than one size than please fill the size1 and 2 prices also orderly. ');">
                    </div>

                    <i class="fa fa-money prefix grey-text"></i>
                    <label for="orangeForm-price">Price</label>
                    <div class="form-group">

                        <input type="text" id="orangeForm-price" name="price" class="form-control">

                    </div>


                    <div class="text-center">
                        <button class="btn btn-outline-primary" name="submit" type="submit">Update Items</button>
                        <button type="reset" class="btn btn-outline-danger btn-lg">cancel</button>
                    </div>
                </form>

            </div>

        </div>

    </div>


    <div class="col-md-3"></div>

</div>
<!--/main body page-->


@endsection