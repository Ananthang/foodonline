<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
     <meta name="csrf-token" content="{{ csrf_token() }}" />
    <title>@yield('title')</title>
    <!-- Font ../Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="/css/font.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <link href="/css/datatables.min.css" rel="stylesheet">
    <link href="/css/jquery-ui.css" rel="stylesheet">
    <!-- Material Design Bootstrap -->
    <link href="/css/mert.min.css" rel="stylesheet">
    <link href="/css/waves.min.css" rel="stylesheet">
    <link href="/css/slider.css" rel="stylesheet">
    <link href="/css/animate.css" rel="stylesheet">
  

 
<!-- Include Date Range Picker -->

<link rel="stylesheet" type="text/css" href="/css/daterangepicker.css" />


    <!-- Your custom styles (optional) -->
    <link href="/css/styleuser.css" rel="stylesheet">
    <link href="/css/customboostrap.css" rel="stylesheet">
    <!--icon Design -->
    <link rel="stylesheet" href="/css/icons/icomoon/styles.css">
    
    
    <link rel="calander" href="https://calendar.google.com/calendar?cid=b25wNjR0cGNyODJnZWExYnVsZjM2bzkyNmNAZ3JvdXAuY2FsZW5kYXIuZ29vZ2xlLmNvbQ">



</head>

<body data-spy="scroll" data-target=".navbar" data-offset="50">


    <h4><strong>Dear Customer,</strong></h4>

    Your Food Order's Accepted from our shop.

    Will Start Proccess soon.
    
    <center>Thank You for Joining with us.</center>

    Your Regards,
    Shop's Owner.


<script type="text/javascript" src="/js/jquery.min.js"></script>
  
<script type="text/javascript" src="/js/bootstrap.min.js"></script>
<script type="text/javascript" src="/js/waves.min.js"></script>

</body>

</html>