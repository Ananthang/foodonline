@extends('layouts.user') @section('title','Food Ordering') @section('content')
<br>
<br>
<br>
  <div class="card text-center">
    <div class="card-header">
      <h2>
        <strong>
          <center>Order List</center>
        </strong>
      </h2>
    </div>
    <br>

    <div class="card-block">
      <p class="card-text">
        <table class="table table-striped table-bordered table-hover addCartList">
          <thead class="">
            <tr class="">
              <th>Food Name</th>
              <th>Image</th>
              <th>Size</th>
              <th class="quantityTitle">Qty</th>
              <th>Price (Rs.)</th>
              <th>Update</th>
              <th>delete</th>
            </tr>
          </thead>
          <tbody>
            @foreach($orderTs as $orderT)

            <tr>
              <td>{{ $orderT->foodname }} {{$orderT->id}}</td>
              <td>
                <img src="/storage/upload/foodImage/{{$orderT->foodImg}}" id="showImage" class="img-fluid" alt="{{$orderT->foodname}}"></img>
              </td>

              <td>{{ $orderT->size_name }}</td>
              <td>
                <input type="text" value="{{$orderT->quantity}}" placeholder="Qty" id="quantity{{$orderT->id}}" data-id="{{$orderT->id}}"
                  class="form-control quantity">
              </td>
              <td id="price{{$orderT->id}}" data-id="{{ $orderT->price}}" data-last-value="{{ $orderT->id }}" class="orderTPrice">
                <?php 
           $mut  = (int) $orderT->price*(int)$orderT->quantity
             ?>{{$mut}}</td>
              <td>

                <a id="updateAddId{{ $orderT->id }}" data-id="{{ $orderT->id }}" class="updateAddCart">
                  <img id="modalImage" src="/img/user/Text-Edit-icon.png">
                </a id="">

              </td>
              <td>
                <a id="deleteAddId{{ $orderT->id }}" data-id="{{ $orderT->id }}" class="deleteAddCart">
                  <img id="modalImage" src="/img/user/delete-icon.png">
                </a>
              </td>


            </tr>
            @endforeach
          </tbody>

        </table>
        <table class="table table-striped table-bordered table-hover">
          <thead class="">
            <tr class="">
              <th>
                <strong>Total (Rs.)</strong>
              </th>
              <th id="total"></th>

            </tr>
          </thead>

        </table>
      </p>
    </div>
  </div>
  <br>
  </div>
  <br>
  <br>
  <div class="form card">
    <br>
    <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
      <strong><center><a class="nav-link active" id="v-pills-doordelivery-tab" href="#v-pills-doordelivery" onclick="doordeliveryfunction();" data-toggle="pill"
        role="tab" aria-controls="v-pills-doordelivery" aria-selected="true">Door Delivery</a></center></strong>

      <a class="nav-link" id="v-pills-takeaway-tab" href="#v-pills-takeaway" onclick="takeawayfunction();" data-toggle="pill"  role="tab"
        aria-controls="v-pills-takeaway" aria-selected="false"> <strong><center>Take Away</center></strong></a>

    </div>
    <!--Door Delivery -->
    <div class="tab-content hide" id="v-pills-tabContent">
      <div class="tab-pane fade show active" id="v-pills-doordelivery"  role="tabpanel" aria-labelledby="v-pills-doordelivery-tab">

        <div class="orderform">
          <div id = "doordeliveryform" autocomplete="off">
           
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" id="nameD" name="name" class="form-control">
            </div>
            <div class="form-group">
              <label for="phonenumber">Phone Number</label>
              <input type="text" id="phonenumberD" name="phonenumber" class="form-control"><small>Phone number must be 10 digits</small>
            </div>
             <div class="form-group">
              <label for="phonenumber">Email Address</label>
              <input type="text" id = "emailaddressD" name="emailaddress" class="form-control"><small>Email address Eg:- ********@****.com</small
            </div>
            <div class="form-group">
              <label for="address">Delivery Address</label>
              <input type="text" name="address" id = "addressD" class="form-control">

            </div>>
            <div class="form-group">
              <label for="city">City</label>
              <input type="text" name="city" id = "cityD" class="form-control">
            </div>

            <div class="form-group form-check deliverynowId" id="">
              <input type="checkbox" class="form-check-input deliverynow" id="">
              <label class="form-check-label" for="exampleCheck1">Delivery Now</label>
              <br>
              <small>This will place order now</small>
            </div>
            <div class="form-group form-check deliverylaterId" id="">
              <input type="checkbox" class="form-check-input deliverylater" id="deliverylaterD">
              <label class="form-check-label" for="exampleCheck1">Delivery Later</label>
            </div>
            <div class="hide orderPickers" id="orderPickers">
              <div class="form-group">
                <input type="text" class="form-control datetimepicker" name="date" id="datetimepickerdoor">
                <br>
                <small>Pick the date and time when you want to place the order</small>
              </div>

            </div>
            <center>
            <a class="btn btn-success orderplacedoordelivery" id="submit" data-id="Door_Delivery" name = "submit" type="submit">submit</a>
            <a class="btn btn-danger " id = "reset" type="reset">Reset</a>
                         <a class="btn btn-danger hide" href="/" id="homeback" type="reset">Home Page</a>
</center>
</div>
        </div>

      </div>

      <div class="tab-pane fade" id="v-pills-takeaway"  role="tabpanel" aria-labelledby="v-pills-takeaway-tab">

        <div class="orderform">
          <form method="POST" action="/order" id = "takeawayform" autocomplete="off">
            {{csrf_field() }}
            <div class="form-group">
              <label for="name">Name</label>
              <input type="text" id="namet" name="name" class="form-control">
            </div>
            <div class="form-group">
              <label for="phonenumber">Phone Number</label>
              <input type="text" id = "phonenumbert" name="phonenumber" class="form-control"><small>Phone number must be 10 digits</small>
            </div>
            
            <div class="form-group">
              <label for="phonenumber">Email Address</label>
              <input type="text" id = "emailaddresst" name="emailaddress" class="form-control"> <small>Email address Eg:- ********@****.com</small>
            </div>


            <div class="form-group form-check deliverynowId" id="">
              <input type="checkbox" class="form-check-input deliverynow" id="" name = "delivertype">
              <label class="form-check-label" for="exampleCheck1">Delivery Now</label>
              <br>
              <small>This will place order now</small>
            </div>
            <div class="form-group form-check deliverylaterId" id="">
              <input type="checkbox" class="form-check-input deliverylater" id="deliverylatertake" name = "delivertype">
              <label class="form-check-label" for="exampleCheck1">Delivery Later</label>
            </div>
            <div class="hide orderPickers" id="orderPickers">
              <div class="form-group">
                <input type="text" class="form-control datetimepicker" name="date" id="datetimepickertake">
                <br>
                <small>Pick the date and time when you want to place the order</small>
              </div>

            </div>
           <center>
            <a class="btn btn-success orderplacetakeaway show" id="submit" data-id="Take_Away" type="submit ">submit</a>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;
            <a class="btn btn-danger show" id = "reset" type="reset">Reset</a>
            <a class="btn btn-danger hide" href="/" id="homeback" type="reset">Home Page</a>

</center>

          </form>
        </div>

      </div>

    </div>


<br>


  </div>



  @endsection