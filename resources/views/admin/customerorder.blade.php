@extends('layouts.homenav')


@section('content')
  <br>
                    <br>
                    <br>
    
    <div class="card">
        <br>
        <p>
             <table class="table table-striped table-bordered table-hover " id="TableId">
                <thead class="">
                    <tr class="">
                        <th>Order Id</th>
                        <th>Customer Name</th>
                        <th>Customer Phone Number</th>
                        <th>Order Type</th>
                        <th>Deleivery Type</th>
                        <th>Status</th>
                        <th>More Details</th>
                        <th>Update</th>
                    </tr>
                </thead>
                <tbody>
                    @foreach($orders as $order)

                    <tr>
                        <td>{{$order->id}}</td>
                        <td>{{$order->customername}}</td>
                        <td>
                            {{$order->phoneno}}
                        </td>
                        <td>{{$order->order_type}}</td>
                        <td>{{$order->delivery_type}}</td>
                        <td>{{$order->status}}</td>
                        <td>
                            <a class="btn btn-success moreDatails" href="/admin/home/order/{{ $order->id }}/editOrder" data-id = "{{$order->id}}">More Details</a>
                        </td>
                        <td>
                             <a class="btn btn-warning " href="/admin/home/order/{{ $order->id }}/editOrder" data-id = "{{$order->id}}">Update</a>
                        </td>
                        


                    </tr>
                    @endforeach
                </tbody>

            </table>
        </p>
        
        <!--tap panel content show description-->
    </div>


             
@endsection