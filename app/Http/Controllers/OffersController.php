<?php

namespace App\Http\Controllers;

use App\Offers;
use App\Auth;
use App\User;
use App\FoodType;
use App\Shop;
use App\FoodItem;
use App\FoodSize;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Redirect;

class OffersController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        if(session()->has('shop_id')){

           $shop_id = session('shop_id');
          $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
            'offers'   =>  DB::table('food_items')
            
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('offers','food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.size_name','food_sizes.price','food_sizes.id','offers.orignal_price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
                'shops' =>Shop::find(session()->get('shop_id')),  
);
        return view ('admin.offers')->with($data);}
       else{
           return Redirect::to('/admin');
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        if(session()->has('shop_id')){

        $shop_id = session()->get('shop_id');
        $food_sizes_id = $request->id;
        $orignal_price = $request->orignal_price;
        $offer_price = $request->offer_price;

        $food_sizes = FoodSize::find($food_sizes_id);

        
            $size_name = $food_sizes->size_name;
            $food_items_id = $food_sizes->food_items_id;
            $price = $food_sizes->price;
      

        $food_sizes->size_name = $size_name;
        $food_sizes->food_items_id = $food_items_id;
        $food_sizes->price = $offer_price;

        $food_sizes->save();
        
        $offer = new Offers;
        $offer->food_Sizes_id = $food_sizes_id;
        $offer->shop_id = $shop_id;
        $offer->orignal_price = $orignal_price;

        $offer->save();

           $shop_id = session('shop_id');
        $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
            'offers'   =>  DB::table('food_items')
            
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('offers','food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
                'shops' =>Shop::find(session()->get('shop_id')),  
);

        return $data;
}
       else{
           return Redirect::to('/admin');
       }
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Offers  $offers
     * @return \Illuminate\Http\Response
     */
    public function show(Offers $offers)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Offers  $offers
     * @return \Illuminate\Http\Response
     */
    public function edit(Offers $offers)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Offers  $offers
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Offers $offers)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Offers  $offers
     * @return \Illuminate\Http\Response
     */
    public function destroy(Offers $offers)
    {
        //
    }
    public function destroyItem($id)
    {
        if(session()->has('shop_id')){

        $offer = Offers::find($id);

        $food_sizes_id = $offer->food_sizes_id;
        $orignal_price = $offer->orignal_price;
        $food_sizes = FoodSize::find($food_sizes_id);

       
            $size_name = $food_sizes->size_name;
            $food_items_id = $food_sizes->food_items_id;
            $price = $food_sizes->price;

            $food_sizes->size_name=$size_name;
            $food_sizes->food_items_id=$food_items_id;
            $food_sizes->price=$orignal_price;

            
            $offer->delete();
        $food_sizes->save();
    $shop_id = session('shop_id');
        $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
            'offers'   =>  DB::table('food_items')
            
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('offers','food_sizes_id','=','food_sizes.id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->get(),
                'shops' =>Shop::find(session()->get('shop_id')),  
);

        return $data;

}
       else{
           return Redirect::to('/admin');
       }
    }
}
