<?php

namespace App;
use Laravel\Scout\Searchable;
use Illuminate\Database\Eloquent\Model;

class FoodItem extends Model
{

     use Searchable;


    protected $fillable = [
    'foodname' , 'foodImg' , 'description' , 'food_types_id' ,
    ];
public function searchableAs()
    {
        return 'foodname';
    }
    public function foodsize()
    {
        return $this->belongsToMany('App\FoodSize','food_items_id');
    }


    public function foodtype()
    {
        return $this->belongTo('App\FoodType','food_types_id');
    }

}
