<?php

namespace App\Http\Controllers;

use App\Auth;
use App\User;
use App\FoodType;
use App\Shop;
use App\FoodItem;
use App\FoodSize;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;

class userFoodController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        
          
        $shop_id = session('shop_id');
        $data = array(
            'foodtypes'=>DB::table('food_types')
                                    ->where('shop_id' ,'=',$shop_id)
                                    ->get(),
            'foods'   =>  DB::table('food_items')
            ->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')
            ->join('food_types' , 'food_items.food_types_id','=','food_types.id')
            ->select('food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.id','food_sizes.size_name','food_sizes.price')
            
            ->where('food_types.shop_id','=',session()->get('shop_id'))
            ->paginate(10),
                'shops' =>Shop::find(session()->get('shop_id')),
);
      
           

                return view('users.fooditems')->with($data);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
