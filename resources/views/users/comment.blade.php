@extends('layouts.user')

@section('title','User Comment')

@section('content')
<br>
<br>
<br>
<div class="card text-center">
  <div class="card-header">
    <h2>
      <strong> Comments</strong>
    </h2>
  </div>
  <br>
  <div class="card-block">
    
    <div class="card-block" id="commentTable">
        
         <table class="table table-striped table-bordered table-hover display commentTable" id = "commentTableId" >
           <thead>
             
              <th>Email Address</th>
              <th>Comment</th>
              <th>Time</th>
            </thead><tbody>
            @foreach($comments as $comment)
            <tr>
             
                 
                 <td id = "commentImaEmail">{{ $comment->emailaddress }}</td>
                 <td>{{ $comment->comment }} </td>
                 <td id = "commentTime">{{ $comment->created_at->diffForHumans() }} </td>
             </tr>
            @endforeach
          </tbody>
         </table>
     
    </div>
  </div>
</div>

<br>
<div class="card text-center" id="comment">
  <div class="card-header">
    <h2>
      <strong> Leave a Comment </strong>
    </h2>
  </div>
  <br>
  <div class="card-block">
    <div class="card-block">
      <p class="card-text">
        <div class="form-group" id = "commentform">

         
            <div class="column is-12">
              <label class="label"  for="email">Email</label>
             
                <input class="form-control" id = "commentEmail"  name="email" type="text" placeholder="Email">
              
              
            </div>
            <br>
            <div class="form-group">
              <textarea type="text" name="comment" id = "commentText" class="form-control" placeholder="Comments here........"></textarea>
            </div>
          <input type = "hidden" name = "shop_id" id = "commentShop_id" value="{{ $shops->id }}" >
            <a  class="btn btn-green btn-lg commentSubmit">submit</a>

          
          <button href="/comment" class="btn btn-info">View Comments</button>
        </div>
      </p>
    </div>
  </div>
</div>



@endsection