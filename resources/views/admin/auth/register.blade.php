<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
    <meta http-equiv="x-ua-compatible" content="ie=edge">
    <title>Shopadmin Profile Register</title>
    <!-- Font Awesome -->
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css">
    <link rel="stylesheet" href="css/font.min.css">
    <!-- Bootstrap core CSS -->
    <link href="/css/bootstrap.min.css" rel="stylesheet">
    <link href="/css/bootstrap.css" rel="stylesheet">
    <!-- Your custom styles (optional) -->
    <link href="/css/style.css" rel="stylesheet">
    <!--icon Design -->
    <link href="/css/stylelogin.css" rel="stylesheet">
    <link rel="stylesheet" href="css/icons/icomoon/styles.css">
    
</head>

<body>

    <!-- Start your project here-->
    <!-- SideNav slide-out button -->
    <header>

       

        @if(session('shop'))
    <div>
        <p class="alert alert-success">
            {{session('shop')}}
        </p>
    </div>
@endif

    </header>
    <!--/. Sidebar navigation -->
    <!-- /Start your project here-->
    <main>

        <!--main body page-->


        <div class="row">

            <div class="col-md-3 col-lg-3 col-sm-3">

            </div>
            <div class="col-md-6 col-lg-6 col-sm-6">
                <br>
                <br>
                <br>
                <div class="card card cascade">
                    <div class="view overlay hm-white-slight">
                        <div>
                            <form action="reg" method="POST" id="formReg" enctype="multipart/form-data">
                                {{csrf_field() }}
                                <p class="h3 text-center mb-4 thicker">Registration Form</p>
                                <input name ='shop_id' type="hidden" value="1"><!--{{session('id')}}-->

       
                                <div class="form-group">
                                    <i class="fa fa-user prefix grey-text"></i> 
                                    <label for="orangeForm-name">Name</label>
                                    <input type="text" id="orangeForm-name" name="name" class="form-control">
                                   
                                </div>

                                <div class="form-group">
                                    <i class="fa fa-user prefix grey-text"></i> 
                                    <label for="orangeForm-username">Username</label>
                                    <input type="text" id="orangeForm-username" name="username" class="form-control">
                                   
                                </div>

                                <div class="form-group">
                                    <i class="fa fa-image prefix grey-text"></i><label for="orangeForm-userImg">Profile picture</label>
                                    <input type="file" id="orangeForm-userImg" name="userImg" class="form-control">
                                    
                                </div>



                                <div class=" form-group {{ $errors->has('password') ? ' has-error' : '' }}">
                                    <i class="fa fa-key prefix grey-text"></i>
                                    <label for="orangeForm-password" class="col-md-4 control-label">Password</label>
                                </div>

                                    <input id="orangeForm-password" type="password" class="form-control" name="password" required> 
                                    @if ($errors->has('password'))
                                    <span class="help-block">
                                        <strong>{{ $errors->first('password') }}</strong>
                                    </span>
                                    @endif

                                </div>

                                <div class="form-group">
                                    <i class="fa fa-key prefix grey-text"></i>
                                    <label for="orangeForm-password-confirm" class="col-md-4 control-label">Confirm Password</label>
                                    <input id="orangeForm-password-confirm" type="password" class="form-control" name="password_confirmation" required>

                                </div>


                                <div class="text-center">
                                    <button class="btn btn-primary" name="submit" type="submit">Create Owner</button>
                                </div>

                            </form>
                            <!-- Form register -->
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-3 col-lg-3 col-sm-3">
                <br>
                <br>
                <br>
                <br>
                <br>
                <br> @if ($errors->any()) @foreach ($errors->all() as $error)
                <div class="alert alert-danger">
                    {{ $error }}
                </div>
                @endforeach @endif
            </div>
        </div>

        <!--/main body page-->
        </div>
        </div>
    </main>
    <!--/main-->
    <footer>


    </footer>
    <!-- SCRIPTS -->
    <!-- JQuery -->
    <script>
    </script>
    <script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
    <!-- Bootstrap tooltips -->
    <script type="text/javascript" src="js/popper.min.js"></script>
    <!-- Bootstrap core JavaScript -->
    <script type="text/javascript" src="js/bootstrap.min.js"></script>
    <script type="text/javascript" src="js/bootstrapc.min.js"></script>
    <!-- MDB core JavaScript -->

    <script src="js/custom.js"></script>
    <script>
        /* $(document).ready(function() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 $('.dropp').slideUp("slow");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              $(".dropper").click(function() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                  $(".dropp").slideToggle("slow");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                              });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          });
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                          /*  function drop() {
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                               $("#dropp").slideUp("slow");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                alert("ahsfdad");
                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                     }*/
        $("#slidenava").sideNav();
        // SideNav Options
        $('#slidenava').sideNav({
            edge: 'right', // Choose the horizontal origin
            closeOnClick: true // Closes side-nav on <a> clicks, useful for Angular/Meteor
        });

        // Show sideNav
        $('#slidenava-item').sideNav('hide');
        // Hide sideNav

        $('#slidenava-item').sideNav('show');
    </script>

</body>

</html>