<?php

namespace App\Http\Controllers;

use App\FoodSize;

use App\Auth;
use App\User;
use App\FoodType;
use App\Shop;
use App\FoodItem;
use Illuminate\Http\Request;
use Image;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Storage;
use Session;
use File;
use Illuminate\Support\Facades\DB;
use Illuminate\Http\UploadedFile;
use Illuminate\Support\Facades\Redirect;

class FoodSizeController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {if(session()->has('shop_id')){

                     $data = array(
   'foodtypes'  =>  FoodType::all(),
   'foods'   =>  DB::table('food_items')->join('food_sizes', 'food_items.id', '=', 'food_sizes.food_items_id')->join('food_types' , 'food_items.food_types_id','=','food_types.id')->select('food_items.id','food_items.foodname','food_items.foodImg','food_items.description','food_types.typename','food_sizes.quantity_name','food_sizes.price')->get(),
    'shops' =>Shop::find(session()->get('shop_id')),
     'item' => FoodItem::all(),

);

        return view('admin.size')->with($data);
    }
       else{
           return Redirect::to('/admin');
       }
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\FoodSize  $foodSize
     * @return \Illuminate\Http\Response
     */
    public function show(FoodSize $foodSize)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\FoodSize  $foodSize
     * @return \Illuminate\Http\Response
     */
    public function edit(FoodSize $foodSize)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\FoodSize  $foodSize
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, FoodSize $foodSize)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\FoodSize  $foodSize
     * @return \Illuminate\Http\Response
     */
    public function destroy(FoodSize $foodSize)
    {
        //
    }
}
